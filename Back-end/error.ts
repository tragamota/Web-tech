export interface Error {
  message: string;
  code: number;
  Datetime: string;
}
