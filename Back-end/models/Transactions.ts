export default interface Register {
  from: string;
  to: string;
  price: Number;
}
