import mongoose from "mongoose";

export default interface Register {
  id?: string;
}

export const RegisterSchema = mongoose.model("register", new mongoose.Schema({
  id: String,
}));
