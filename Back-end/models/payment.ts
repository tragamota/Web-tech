import mongoose from "mongoose";

export enum PaymentCategory {
  Bioscoop = "Bioscoop",
  Restaurant = "Restaurant",
  Karting = "Karten",
  Museum = "Museum",
  AdvancedPayment = "Voorgeschoten",
  Other = "Overig",
}

export interface Payment {
  id?: string;
  from: string;
  price: number;
  category: PaymentCategory;
  description?: string;
}

export const paymentSchema = mongoose.model("payment",new mongoose.Schema({
  id: String,
  from: String,
  price: Number,
  category: String,
  description: String,
  registerId: String,
}));