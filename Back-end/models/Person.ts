import mongoose from "mongoose";

export default interface Person {
  id?: string;
  name: string;
}

export const personSchema = mongoose.model("person", new mongoose.Schema({
  id: String,
  name: String,
  registerId: String,
}));
