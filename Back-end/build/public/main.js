"use strict";
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
        /***/ 0: 
        /*!***************************!*\
          !*** multi ./src/main.ts ***!
          \***************************/
        /*! no static exports found */
        /***/ (function (module, exports, __webpack_require__) {
            module.exports = __webpack_require__(/*! C:\Users\Ian\Documents\Gitlab\Web-tech\angular\angular-front-end\src\main.ts */ "zUnb");
            /***/ 
        }),
        /***/ "2Rw8": 
        /*!*******************************************************!*\
          !*** ./src/app/components/person/person.component.ts ***!
          \*******************************************************/
        /*! exports provided: PersonComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PersonComponent", function () { return PersonComponent; });
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
            /* harmony import */ var src_app_services_peopleService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/peopleService */ "T0Np");
            class PersonComponent {
                constructor(peopleService) {
                    this.peopleService = peopleService;
                }
                ngOnInit() {
                }
                deletePerson() {
                    this.peopleService.deletePerson(this.person);
                }
            }
            PersonComponent.ɵfac = function PersonComponent_Factory(t) { return new (t || PersonComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_peopleService__WEBPACK_IMPORTED_MODULE_1__["PeopleService"])); };
            PersonComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PersonComponent, selectors: [["app-person"]], inputs: { person: "person" }, decls: 9, vars: 1, consts: [[1, "card"], [1, "card-body"], [1, "payment-delete-body"], [1, "material-icons", "payment-delete-remove", 3, "click"]], template: function PersonComponent_Template(rf, ctx) {
                    if (rf & 1) {
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "small");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Naam:");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h2");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 2);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "i", 3);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PersonComponent_Template_i_click_7_listener() { return ctx.deletePerson(); });
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " close ");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                    }
                    if (rf & 2) {
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.person.name);
                    }
                }, styles: [".payment-delete-body[_ngcontent-%COMP%] {\n  display: block;\n  position: absolute;\n  margin: 0;\n  top: 0;\n  right: 0;\n  z-index: 1;\n}\n\n.payment-delete-remove[_ngcontent-%COMP%] {\n  cursor: pointer;\n  padding: 0.5rem 0.5rem;\n}\n\n.payment-delete-remove[_ngcontent-%COMP%]:hover {\n  color: #b31616;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxkaXN0XFxhbmd1bGFyLWZyb250LWVuZFxccGVyc29uLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBRUEsU0FBQTtFQUVBLE1BQUE7RUFDQSxRQUFBO0VBRUEsVUFBQTtBQUZGOztBQUtBO0VBQ0UsZUFBQTtFQUVBLHNCQUFBO0FBSEY7O0FBS0U7RUFDRSxjQUFBO0FBSEoiLCJmaWxlIjoicGVyc29uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnBheW1lbnQtZGVsZXRlLWJvZHkge1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IGFic29sdXRlO1xuXG4gIG1hcmdpbjogMDtcblxuICB0b3A6IDA7XG4gIHJpZ2h0OiAwO1xuXG4gIHotaW5kZXg6IDE7XG59XG5cbi5wYXltZW50LWRlbGV0ZS1yZW1vdmUge1xuICBjdXJzb3I6IHBvaW50ZXI7XG5cbiAgcGFkZGluZzogMC41cmVtIDAuNXJlbTtcblxuICAmOmhvdmVyIHtcbiAgICBjb2xvcjogI2IzMTYxNjtcbiAgfVxufVxuIl19 */"] });
            /***/ 
        }),
        /***/ "3STb": 
        /*!*********************************************!*\
          !*** ./src/app/services/registerService.ts ***!
          \*********************************************/
        /*! exports provided: RegisterService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterService", function () { return RegisterService; });
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
            class RegisterService {
                constructor(http) {
                    this.http = http;
                    this.registerUrl = '/api/register';
                    this.httpOptions = {
                        headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]({ 'Content-Type': 'application/json' }),
                    };
                }
                doesRegisterExist(registerkey) {
                    const url = `${this.registerUrl}?payback=${registerkey}`;
                    console.log("checking register key");
                    return this.http.get(url).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["tap"])((x) => {
                        console.log(x);
                        console.log("register does exist");
                        this.registerId = registerkey;
                    }));
                }
                createRegister() {
                    const url = `${this.registerUrl}`;
                    console.log("checking register key");
                    return this.http.post(url, '');
                }
            }
            RegisterService.ɵfac = function RegisterService_Factory(t) { return new (t || RegisterService)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"])); };
            RegisterService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjectable"]({ token: RegisterService, factory: RegisterService.ɵfac, providedIn: 'root' });
            /***/ 
        }),
        /***/ "9hoZ": 
        /*!*************************************************************************!*\
          !*** ./src/app/components/people-overview/people-overview.component.ts ***!
          \*************************************************************************/
        /*! exports provided: PeopleOverviewComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PeopleOverviewComponent", function () { return PeopleOverviewComponent; });
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
            /* harmony import */ var src_app_services_peopleService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/peopleService */ "T0Np");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
            /* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");
            /* harmony import */ var _person_person_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../person/person.component */ "2Rw8");
            function PeopleOverviewComponent_ol_7_li_1_Template(rf, ctx) {
                if (rf & 1) {
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li", 8);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-person", 9);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                }
                if (rf & 2) {
                    const person_r3 = ctx.$implicit;
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("person", person_r3);
                }
            }
            function PeopleOverviewComponent_ol_7_Template(rf, ctx) {
                if (rf & 1) {
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ol", 6);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, PeopleOverviewComponent_ol_7_li_1_Template, 2, 1, "li", 7);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                }
                if (rf & 2) {
                    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r0.people);
                }
            }
            function PeopleOverviewComponent_div_8_Template(rf, ctx) {
                if (rf & 1) {
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 10);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span");
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Er zijn nog geen leden ingevoerd");
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                }
            }
            class PeopleOverviewComponent {
                constructor(peopleService) {
                    this.peopleService = peopleService;
                    this.people = [];
                }
                ngOnInit() {
                    this.peopleService.getPeople().subscribe(x => {
                        this.people = x;
                    });
                }
            }
            PeopleOverviewComponent.ɵfac = function PeopleOverviewComponent_Factory(t) { return new (t || PeopleOverviewComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_peopleService__WEBPACK_IMPORTED_MODULE_1__["PeopleService"])); };
            PeopleOverviewComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PeopleOverviewComponent, selectors: [["app-people-overview"]], decls: 9, vars: 2, consts: [[1, "content-container", "people-wrapper"], [1, "people-header"], ["routerLink", "/people", 1, "material-icons", "people", "people-header-add"], [1, "people-container"], ["class", "people-list", 4, "ngIf"], ["class", "people-empty", 4, "ngIf"], [1, "people-list"], ["class", "people-item", 4, "ngFor", "ngForOf"], [1, "people-item"], [3, "person"], [1, "people-empty"]], template: function PeopleOverviewComponent_Template(rf, ctx) {
                    if (rf & 1) {
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Pool");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "a", 2);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " add_box ");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 3);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, PeopleOverviewComponent_ol_7_Template, 2, 1, "ol", 4);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, PeopleOverviewComponent_div_8_Template, 3, 0, "div", 5);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                    }
                    if (rf & 2) {
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.people.length !== 0);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.people.length === 0);
                    }
                }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"], _person_person_component__WEBPACK_IMPORTED_MODULE_4__["PersonComponent"]], styles: [".people-wrapper[_ngcontent-%COMP%] {\n  margin-top: 2rem;\n  min-height: 15rem;\n}\n\n.people-container[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  width: 100%;\n  margin-bottom: 1.5rem;\n}\n\n.people-header[_ngcontent-%COMP%] {\n  display: flex;\n  position: relative;\n  justify-content: space-between;\n  align-items: center;\n}\n\n.people-header-add[_ngcontent-%COMP%] {\n  color: #000000;\n  padding: 0.4rem;\n  text-decoration: none;\n}\n\n.people-list[_ngcontent-%COMP%] {\n  display: grid;\n  position: relative;\n  flex-wrap: wrap;\n  grid-template-columns: repeat(4, 1fr);\n  gap: 1rem 0.75rem;\n  padding: 0;\n  margin-top: 2rem;\n  margin-bottom: 1rem;\n}\n\n.people-item[_ngcontent-%COMP%] {\n  margin: 0;\n  padding: 0;\n  flex: 1 0 25%;\n  list-style-type: none;\n}\n\n.people-empty[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  font-size: 20px;\n  margin-top: 1.5rem;\n  margin-bottom: 1.5rem;\n}\n\n@media screen and (max-width: 992px) {\n  .people-list[_ngcontent-%COMP%] {\n    grid-template-columns: repeat(1, 1fr);\n    gap: 0.5rem 0.5rem;\n  }\n}\n\n@media screen and (min-width: 992px) {\n  .people-list[_ngcontent-%COMP%] {\n    grid-template-columns: repeat(3, 1fr);\n  }\n}\n\n@media screen and (min-width: 1200px) {\n  .people-list[_ngcontent-%COMP%] {\n    grid-template-columns: repeat(4, 1fr);\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxkaXN0XFxhbmd1bGFyLWZyb250LWVuZFxccGVvcGxlLW92ZXJ2aWV3LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0JBQUE7RUFFQSxpQkFBQTtBQUFGOztBQUdBO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBRUEsV0FBQTtFQUVBLHFCQUFBO0FBRkY7O0FBS0E7RUFDRSxhQUFBO0VBQ0Esa0JBQUE7RUFFQSw4QkFBQTtFQUNBLG1CQUFBO0FBSEY7O0FBTUE7RUFDRSxjQUFBO0VBRUEsZUFBQTtFQUVBLHFCQUFBO0FBTEY7O0FBUUE7RUFDRSxhQUFBO0VBQ0Esa0JBQUE7RUFFQSxlQUFBO0VBRUEscUNBQUE7RUFDQSxpQkFBQTtFQUVBLFVBQUE7RUFFQSxnQkFBQTtFQUNBLG1CQUFBO0FBVEY7O0FBWUE7RUFDRSxTQUFBO0VBQ0EsVUFBQTtFQUVBLGFBQUE7RUFDQSxxQkFBQTtBQVZGOztBQWFBO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBRUEsZUFBQTtFQUVBLGtCQUFBO0VBQ0EscUJBQUE7QUFaRjs7QUFlQTtFQUNFO0lBQ0UscUNBQUE7SUFDQSxrQkFBQTtFQVpGO0FBQ0Y7O0FBZUE7RUFDRTtJQUNFLHFDQUFBO0VBYkY7QUFDRjs7QUFnQkE7RUFDRTtJQUNFLHFDQUFBO0VBZEY7QUFDRiIsImZpbGUiOiJwZW9wbGUtb3ZlcnZpZXcuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucGVvcGxlLXdyYXBwZXIge1xuICBtYXJnaW4tdG9wOiAycmVtO1xuXG4gIG1pbi1oZWlnaHQ6IDE1cmVtO1xufVxuXG4ucGVvcGxlLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgd2lkdGg6IDEwMCU7XG5cbiAgbWFyZ2luLWJvdHRvbTogMS41cmVtO1xufVxuXG4ucGVvcGxlLWhlYWRlciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbi5wZW9wbGUtaGVhZGVyLWFkZCB7XG4gIGNvbG9yOiAjMDAwMDAwO1xuXG4gIHBhZGRpbmc6IDAuNHJlbTtcblxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbi5wZW9wbGUtbGlzdCB7XG4gIGRpc3BsYXk6IGdyaWQ7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICBmbGV4LXdyYXA6IHdyYXA7XG5cbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNCwgMWZyKTtcbiAgZ2FwOiAxcmVtIDAuNzVyZW07XG5cbiAgcGFkZGluZzogMDtcblxuICBtYXJnaW4tdG9wOiAycmVtO1xuICBtYXJnaW4tYm90dG9tOiAxcmVtO1xufVxuXG4ucGVvcGxlLWl0ZW0ge1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmc6IDA7XG5cbiAgZmxleDogMSAwIDI1JTtcbiAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xufVxuXG4ucGVvcGxlLWVtcHR5IHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICBmb250LXNpemU6IDIwcHg7XG5cbiAgbWFyZ2luLXRvcDogMS41cmVtO1xuICBtYXJnaW4tYm90dG9tOiAxLjVyZW07XG59XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MnB4KSB7XG4gIC5wZW9wbGUtbGlzdCB7XG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMSwgMWZyKTtcbiAgICBnYXA6IDAuNXJlbSAwLjVyZW07XG4gIH1cbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDogOTkycHgpIHtcbiAgLnBlb3BsZS1saXN0IHtcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgzLCAxZnIpO1xuICB9XG59XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDEyMDBweCkge1xuICAucGVvcGxlLWxpc3Qge1xuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDQsIDFmcik7XG4gIH1cbn1cbiJdfQ== */"] });
            /***/ 
        }),
        /***/ "A0Hx": 
        /*!***********************************************************************************!*\
          !*** ./src/app/components/transaction-overview/transaction-overview.component.ts ***!
          \***********************************************************************************/
        /*! exports provided: TransactionOverviewComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionOverviewComponent", function () { return TransactionOverviewComponent; });
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
            /* harmony import */ var _services_TransactionService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/TransactionService */ "H+Sd");
            /* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
            /* harmony import */ var _transaction_transaction_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../transaction/transaction.component */ "BYgT");
            function TransactionOverviewComponent_ol_7_li_1_Template(rf, ctx) {
                if (rf & 1) {
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li", 8);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-transaction", 9);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                }
                if (rf & 2) {
                    const transaction_r3 = ctx.$implicit;
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("transaction", transaction_r3);
                }
            }
            function TransactionOverviewComponent_ol_7_Template(rf, ctx) {
                if (rf & 1) {
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ol", 6);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TransactionOverviewComponent_ol_7_li_1_Template, 2, 1, "li", 7);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                }
                if (rf & 2) {
                    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r0.transactions);
                }
            }
            function TransactionOverviewComponent_div_8_Template(rf, ctx) {
                if (rf & 1) {
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 10);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span");
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "druk op de refresh knop om de transacties te berekenen");
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                }
            }
            class TransactionOverviewComponent {
                constructor(transactionService) {
                    this.transactionService = transactionService;
                    this.transactions = [];
                }
                ngOnInit() {
                    this.transactionService.getTransactions().subscribe((x) => {
                        this.transactions = x;
                    });
                }
                getTransactions() {
                    this.transactionService.calculateTransaction().subscribe((x) => {
                        this.transactions = x;
                    });
                }
            }
            TransactionOverviewComponent.ɵfac = function TransactionOverviewComponent_Factory(t) { return new (t || TransactionOverviewComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_TransactionService__WEBPACK_IMPORTED_MODULE_1__["TransactionService"])); };
            TransactionOverviewComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: TransactionOverviewComponent, selectors: [["app-transaction-overview"]], decls: 9, vars: 2, consts: [[1, "content-container", "payments-wrapper"], [1, "payments-header"], [1, "material-icons", "payments", "payments-header-add", 3, "click"], [1, "payments-container"], ["class", "payments-list", 4, "ngIf"], ["class", "payments-empty", 4, "ngIf"], [1, "payments-list"], ["class", "payments-item", 4, "ngFor", "ngForOf"], [1, "payments-item"], [3, "transaction"], [1, "payments-empty"]], template: function TransactionOverviewComponent_Template(rf, ctx) {
                    if (rf & 1) {
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Transacties");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 2);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TransactionOverviewComponent_Template_div_click_4_listener() { return ctx.getTransactions(); });
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " loop ");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 3);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, TransactionOverviewComponent_ol_7_Template, 2, 1, "ol", 4);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, TransactionOverviewComponent_div_8_Template, 3, 0, "div", 5);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                    }
                    if (rf & 2) {
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.transactions.length !== 0);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.transactions.length === 0);
                    }
                }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], _transaction_transaction_component__WEBPACK_IMPORTED_MODULE_3__["TransactionComponent"]], styles: [".payments-wrapper[_ngcontent-%COMP%] {\n  margin-top: 2rem;\n  min-height: 15rem;\n}\n\n.payments-container[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  width: 100%;\n  margin-bottom: 1.5rem;\n}\n\n.payments-header[_ngcontent-%COMP%] {\n  display: flex;\n  position: relative;\n  justify-content: space-between;\n  align-items: center;\n}\n\n.payments-header-add[_ngcontent-%COMP%] {\n  color: #000000;\n  padding: 0.4rem;\n  text-decoration: none;\n}\n\n.payments-list[_ngcontent-%COMP%] {\n  display: grid;\n  position: relative;\n  flex-wrap: wrap;\n  grid-template-columns: repeat(4, 1fr);\n  gap: 1rem 0.75rem;\n  padding: 0;\n  margin-top: 2rem;\n  margin-bottom: 1rem;\n}\n\n.payments-item[_ngcontent-%COMP%] {\n  margin: 0;\n  padding: 0;\n  flex: 1 0 25%;\n  list-style-type: none;\n}\n\n.payments-empty[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  font-size: 20px;\n  margin-top: 1.5rem;\n  margin-bottom: 1.5rem;\n}\n\n@media screen and (max-width: 992px) {\n  .payments-list[_ngcontent-%COMP%] {\n    grid-template-columns: repeat(1, 1fr);\n    gap: 0.5rem 0.5rem;\n  }\n}\n\n@media screen and (min-width: 992px) {\n  .payments-list[_ngcontent-%COMP%] {\n    grid-template-columns: repeat(3, 1fr);\n  }\n}\n\n@media screen and (min-width: 1200px) {\n  .payments-list[_ngcontent-%COMP%] {\n    grid-template-columns: repeat(4, 1fr);\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxkaXN0XFxhbmd1bGFyLWZyb250LWVuZFxcdHJhbnNhY3Rpb24tb3ZlcnZpZXcuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtFQUVBLGlCQUFBO0FBQUo7O0FBR0U7RUFDRSxjQUFBO0VBQ0Esa0JBQUE7RUFFQSxXQUFBO0VBRUEscUJBQUE7QUFGSjs7QUFLRTtFQUNFLGFBQUE7RUFDQSxrQkFBQTtFQUVBLDhCQUFBO0VBQ0EsbUJBQUE7QUFISjs7QUFNRTtFQUNFLGNBQUE7RUFFQSxlQUFBO0VBRUEscUJBQUE7QUFMSjs7QUFRRTtFQUNFLGFBQUE7RUFDQSxrQkFBQTtFQUVBLGVBQUE7RUFFQSxxQ0FBQTtFQUNBLGlCQUFBO0VBRUEsVUFBQTtFQUVBLGdCQUFBO0VBQ0EsbUJBQUE7QUFUSjs7QUFZRTtFQUNFLFNBQUE7RUFDQSxVQUFBO0VBRUEsYUFBQTtFQUNBLHFCQUFBO0FBVko7O0FBYUU7RUFDRSxjQUFBO0VBQ0Esa0JBQUE7RUFFQSxlQUFBO0VBRUEsa0JBQUE7RUFDQSxxQkFBQTtBQVpKOztBQWVFO0VBQ0U7SUFDRSxxQ0FBQTtJQUNBLGtCQUFBO0VBWko7QUFDRjs7QUFlRTtFQUNFO0lBQ0UscUNBQUE7RUFiSjtBQUNGOztBQWdCRTtFQUNFO0lBQ0UscUNBQUE7RUFkSjtBQUNGIiwiZmlsZSI6InRyYW5zYWN0aW9uLW92ZXJ2aWV3LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnBheW1lbnRzLXdyYXBwZXIge1xuICAgIG1hcmdpbi10b3A6IDJyZW07XG4gIFxuICAgIG1pbi1oZWlnaHQ6IDE1cmVtO1xuICB9XG4gIFxuICAucGF5bWVudHMtY29udGFpbmVyIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIFxuICAgIHdpZHRoOiAxMDAlO1xuICBcbiAgICBtYXJnaW4tYm90dG9tOiAxLjVyZW07XG4gIH1cbiAgXG4gIC5wYXltZW50cy1oZWFkZXIge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICBcbiAgLnBheW1lbnRzLWhlYWRlci1hZGQge1xuICAgIGNvbG9yOiAjMDAwMDAwO1xuICBcbiAgICBwYWRkaW5nOiAwLjRyZW07XG4gIFxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgfVxuICBcbiAgLnBheW1lbnRzLWxpc3Qge1xuICAgIGRpc3BsYXk6IGdyaWQ7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBcbiAgICBmbGV4LXdyYXA6IHdyYXA7XG4gIFxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDQsIDFmcik7XG4gICAgZ2FwOiAxcmVtIDAuNzVyZW07XG4gIFxuICAgIHBhZGRpbmc6IDA7XG4gIFxuICAgIG1hcmdpbi10b3A6IDJyZW07XG4gICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbiAgfVxuICBcbiAgLnBheW1lbnRzLWl0ZW0ge1xuICAgIG1hcmdpbjogMDtcbiAgICBwYWRkaW5nOiAwO1xuICBcbiAgICBmbGV4OiAxIDAgMjUlO1xuICAgIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgfVxuICBcbiAgLnBheW1lbnRzLWVtcHR5IHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIFxuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgXG4gICAgbWFyZ2luLXRvcDogMS41cmVtO1xuICAgIG1hcmdpbi1ib3R0b206IDEuNXJlbTtcbiAgfVxuICBcbiAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcbiAgICAucGF5bWVudHMtbGlzdCB7XG4gICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgxLCAxZnIpO1xuICAgICAgZ2FwOiAwLjVyZW0gMC41cmVtO1xuICAgIH1cbiAgfVxuICBcbiAgQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDogOTkycHgpIHtcbiAgICAucGF5bWVudHMtbGlzdCB7XG4gICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgzLCAxZnIpO1xuICAgIH1cbiAgfVxuICBcbiAgQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDogMTIwMHB4KSB7XG4gICAgLnBheW1lbnRzLWxpc3Qge1xuICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNCwgMWZyKTtcbiAgICB9XG4gIH1cbiAgIl19 */"] });
            /***/ 
        }),
        /***/ "AytR": 
        /*!*****************************************!*\
          !*** ./src/environments/environment.ts ***!
          \*****************************************/
        /*! exports provided: environment */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function () { return environment; });
            // This file can be replaced during build by using the `fileReplacements` array.
            // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
            // The list of file replacements can be found in `angular.json`.
            const environment = {
                production: false
            };
            /*
             * For easier debugging in development mode, you can import the following file
             * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
             *
             * This import should be commented out in production mode because it will have a negative impact
             * on performance if an error is thrown.
             */
            // import 'zone.js/dist/zone-error';  // Included with Angular CLI.
            /***/ 
        }),
        /***/ "BYgT": 
        /*!*****************************************************************!*\
          !*** ./src/app/components/transaction/transaction.component.ts ***!
          \*****************************************************************/
        /*! exports provided: TransactionComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionComponent", function () { return TransactionComponent; });
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
            class TransactionComponent {
                constructor() { }
                ngOnInit() {
                }
            }
            TransactionComponent.ɵfac = function TransactionComponent_Factory(t) { return new (t || TransactionComponent)(); };
            TransactionComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: TransactionComponent, selectors: [["app-transaction"]], inputs: { transaction: "transaction" }, decls: 21, vars: 3, consts: [[1, "card"], [1, "card-body"], [1, "payment-name"], [1, "payment-general-wrapper"], [1, "payment-general-price"]], template: function TransactionComponent_Template(rf, ctx) {
                    if (rf & 1) {
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "small");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "i");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Van:");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h2");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 2);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "small");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "i");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Naar:");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "h2");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 3);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 4);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "small");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "i");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Hoeveel: ");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "span");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                    }
                    if (rf & 2) {
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.transaction.from);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.transaction.to);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.transaction.price);
                    }
                }, styles: [".payment-card[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  flex-direction: column;\n  word-wrap: break-word;\n  height: 100%;\n  border: 2px solid #455a64;\n  border-radius: 0.2rem;\n}\n\n.card-body[_ngcontent-%COMP%]   small[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  color: #a0a0a0;\n  margin-bottom: 0.75rem;\n}\n\n.payment-container[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  text-decoration: none;\n  color: #000000;\n  padding: 0.5rem 0.5rem;\n  margin: 0.5rem 0.5rem;\n}\n\n.payment-general-wrapper[_ngcontent-%COMP%] {\n  display: flex;\n  position: relative;\n}\n\n.payment-general-price[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  width: 50%;\n}\n\n.payment-general-category[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  width: 50%;\n}\n\n.payment-name[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  color: inherit;\n  margin-top: 0.5rem;\n  margin-bottom: 0.5rem;\n}\n\n.payment-description[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  margin: 0.5rem 0;\n}\n\n.payment-delete-body[_ngcontent-%COMP%] {\n  display: block;\n  position: absolute;\n  margin: 0;\n  top: 0;\n  right: 0;\n  z-index: 1;\n}\n\n.payment-delete-remove[_ngcontent-%COMP%] {\n  cursor: pointer;\n  padding: 0.5rem 0.5rem;\n}\n\n.payment-delete-remove[_ngcontent-%COMP%]:hover {\n  color: #b31616;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxkaXN0XFxhbmd1bGFyLWZyb250LWVuZFxcdHJhbnNhY3Rpb24uY29tcG9uZW50LnNjc3MiLCIuLlxcLi5cXC4uXFwuLlxcLi5cXHZhcmlhYmxlcy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBRUEsc0JBQUE7RUFDQSxxQkFBQTtFQUVBLFlBQUE7RUFFQSx5QkFBQTtFQUNBLHFCQUFBO0FBSkY7O0FBT0E7RUFDRSxjQUFBO0VBQ0Esa0JBQUE7RUFFQSxjQUFBO0VBRUEsc0JBQUE7QUFORjs7QUFTQTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUVBLHFCQUFBO0VBQ0EsY0NyQnFCO0VEdUJyQixzQkFBQTtFQUNBLHFCQUFBO0FBUkY7O0FBV0E7RUFDRSxhQUFBO0VBQ0Esa0JBQUE7QUFSRjs7QUFXQTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUVBLFVBQUE7QUFURjs7QUFZQTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUVBLFVBQUE7QUFWRjs7QUFhQTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUVBLGNBQUE7RUFFQSxrQkFBQTtFQUNBLHFCQUFBO0FBWkY7O0FBZUE7RUFDRSxjQUFBO0VBQ0Esa0JBQUE7RUFFQSxnQkFBQTtBQWJGOztBQWdCQTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUVBLFNBQUE7RUFFQSxNQUFBO0VBQ0EsUUFBQTtFQUVBLFVBQUE7QUFoQkY7O0FBbUJBO0VBQ0UsZUFBQTtFQUVBLHNCQUFBO0FBakJGOztBQW1CRTtFQUNFLGNBQUE7QUFqQkoiLCJmaWxlIjoidHJhbnNhY3Rpb24uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAdXNlICcuLi8uLi8uLi92YXJpYWJsZXMuc2NzcycgYXMgdjtcblxuLnBheW1lbnQtY2FyZCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgd29yZC13cmFwOiBicmVhay13b3JkO1xuXG4gIGhlaWdodDogMTAwJTtcblxuICBib3JkZXI6IDJweCBzb2xpZCB2LiRwcmltYXJ5LWNvbG9yO1xuICBib3JkZXItcmFkaXVzOiAwLjJyZW07XG59XG5cbi5jYXJkLWJvZHkgc21hbGwge1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gIGNvbG9yOiAjYTBhMGEwO1xuXG4gIG1hcmdpbi1ib3R0b206IDAuNzVyZW07XG59XG5cbi5wYXltZW50LWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBjb2xvcjogdi4kc2Vjb25kYXJ5LXRleHQtY29sb3I7XG5cbiAgcGFkZGluZzogMC41cmVtIDAuNXJlbTtcbiAgbWFyZ2luOiAwLjVyZW0gMC41cmVtO1xufVxuXG4ucGF5bWVudC1nZW5lcmFsLXdyYXBwZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5wYXltZW50LWdlbmVyYWwtcHJpY2Uge1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gIHdpZHRoOiA1MCU7XG59XG5cbi5wYXltZW50LWdlbmVyYWwtY2F0ZWdvcnkge1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gIHdpZHRoOiA1MCU7XG59XG5cbi5wYXltZW50LW5hbWUge1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gIGNvbG9yOiBpbmhlcml0O1xuXG4gIG1hcmdpbi10b3A6IDAuNXJlbTtcbiAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xufVxuXG4ucGF5bWVudC1kZXNjcmlwdGlvbiB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgbWFyZ2luOiAwLjVyZW0gMDtcbn1cblxuLnBheW1lbnQtZGVsZXRlLWJvZHkge1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IGFic29sdXRlO1xuXG4gIG1hcmdpbjogMDtcblxuICB0b3A6IDA7XG4gIHJpZ2h0OiAwO1xuXG4gIHotaW5kZXg6IDE7XG59XG5cbi5wYXltZW50LWRlbGV0ZS1yZW1vdmUge1xuICBjdXJzb3I6IHBvaW50ZXI7XG5cbiAgcGFkZGluZzogMC41cmVtIDAuNXJlbTtcblxuICAmOmhvdmVyIHtcbiAgICBjb2xvcjogI2IzMTYxNjtcbiAgfVxufVxuIiwiJHByaW1hcnktY29sb3I6ICM0NTVhNjQ7XG4kcHJpbWFyeS1jb2xvci1saWdodDogIzcxODc5MjtcbiRwcmltYXJ5LWNvbG9yLWRhcms6ICMxYzMxM2E7XG4kcHJpbWFyeS10ZXh0LWNvbG9yOiAjZmZmZmZmO1xuXG4kc2Vjb25kYXJ5LWNvbG9yOiAjZmZhMDAwO1xuJHNlY29uZGFyeS1jb2xvci1saWdodDogI2ZmZDE0OTtcbiRzZWNvbmRhcnktY29sb3ItZGFyazogI2M2NzEwMDtcbiRzZWNvbmRhcnktdGV4dC1jb2xvcjogIzAwMDAwMDtcblxuJHRleHQtc2l6ZTogMTZweDtcbiRib2xkLXNpemU6IDIwcHg7XG4kdGl0bGUtc2l6ZTogMjZweDtcblxuJGJyZWFrcG9pbnRzOiAoNTc2cHgsIDc2OHB4LCA5OTJweCwgMTIwMHB4LCAxNDAwcHgpO1xuJGJyZWFrc2l6ZXM6ICg1NDBweCwgNzIwcHgsIDk2MHB4LCAxMTQwcHgsIDEzMjBweCk7XG5cbiRmb250LWZhbWlseTogJ1JvYm90bycsIHNhbnMtc2VyaWY7XG4kZm9udC13ZWlnaHQ6IDQwMDtcblxuIl19 */"] });
            /***/ 
        }),
        /***/ "C7mK": 
        /*!*************************************!*\
          !*** ./src/app/entities/payment.ts ***!
          \*************************************/
        /*! exports provided: PaymentCategory */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentCategory", function () { return PaymentCategory; });
            var PaymentCategory;
            (function (PaymentCategory) {
                PaymentCategory["Bioscoop"] = "Bioscoop";
                PaymentCategory["Restaurant"] = "Restaurant";
                PaymentCategory["Karting"] = "Karten";
                PaymentCategory["Museum"] = "Museum";
                PaymentCategory["AdvancedPayment"] = "Voorgeschoten";
                PaymentCategory["Other"] = "Overig";
            })(PaymentCategory || (PaymentCategory = {}));
            /***/ 
        }),
        /***/ "H+Sd": 
        /*!************************************************!*\
          !*** ./src/app/services/TransactionService.ts ***!
          \************************************************/
        /*! exports provided: TransactionService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionService", function () { return TransactionService; });
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "qCKp");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
            /* harmony import */ var _registerService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./registerService */ "3STb");
            class TransactionService {
                constructor(http, registerService) {
                    this.http = http;
                    this.registerService = registerService;
                    this.transactionUrl = '/api/transactions';
                    this.transaction = [];
                    this.httpOptions = {
                        headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]({ 'Content-Type': 'application/json' }),
                    };
                }
                getTransactions() {
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(this.transaction);
                }
                calculateTransaction() {
                    const url = `${this.transactionUrl}?payback=${this.registerService.registerId}`;
                    return this.http.get(url).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])((x) => {
                        this.transaction = x;
                    }));
                }
            }
            TransactionService.ɵfac = function TransactionService_Factory(t) { return new (t || TransactionService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_registerService__WEBPACK_IMPORTED_MODULE_4__["RegisterService"])); };
            TransactionService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({ token: TransactionService, factory: TransactionService.ɵfac, providedIn: 'root' });
            /***/ 
        }),
        /***/ "JsTZ": 
        /*!********************************************************************!*\
          !*** ./src/app/views/person-creation/person-creation.component.ts ***!
          \********************************************************************/
        /*! exports provided: PersonCreationComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PersonCreationComponent", function () { return PersonCreationComponent; });
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
            /* harmony import */ var src_app_services_peopleService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/peopleService */ "T0Np");
            /* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");
            /* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
            const _c0 = function () { return { standalone: true }; };
            class PersonCreationComponent {
                constructor(router, personService, location) {
                    this.router = router;
                    this.personService = personService;
                    this.location = location;
                    this.person = {
                        name: '',
                    };
                }
                ngOnInit() { }
                onSubmit() {
                    this.personService.addPerson(this.person).subscribe((result) => {
                        console.log('Added: ');
                        console.log(result);
                        this.router.navigateByUrl('/');
                    }, (error) => {
                        console.log(error);
                    });
                }
                goBack() {
                    this.location.back();
                }
            }
            PersonCreationComponent.ɵfac = function PersonCreationComponent_Factory(t) { return new (t || PersonCreationComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_peopleService__WEBPACK_IMPORTED_MODULE_2__["PeopleService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"])); };
            PersonCreationComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PersonCreationComponent, selectors: [["app-person-creation"]], decls: 15, vars: 3, consts: [[1, "content-container"], [1, "payment-entry-form"], [2, "margin-bottom", "1rem"], ["autocomplete", "off"], [1, "form-group"], ["for", "from"], ["type", "text", "id", "from", 1, "form-control", 3, "ngModel", "ngModelOptions", "ngModelChange"], [1, "entry-actions"], [1, "btn", "btn-primary", 3, "click"]], template: function PersonCreationComponent_Template(rf, ctx) {
                    if (rf & 1) {
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1", 2);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Persoon toevoegen");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "form", 3);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 4);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "label", 5);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Naam");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "input", 6);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function PersonCreationComponent_Template_input_ngModelChange_8_listener($event) { return ctx.person.name = $event; });
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PersonCreationComponent_Template_div_click_10_listener() { return ctx.goBack(); });
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Cancel");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 8);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PersonCreationComponent_Template_div_click_12_listener() { return ctx.onSubmit(); });
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "span");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Persoon toevoegen");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                    }
                    if (rf & 2) {
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.person.name)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](2, _c0));
                    }
                }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgModel"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwZXJzb24tY3JlYXRpb24uY29tcG9uZW50LnNjc3MifQ== */"] });
            /***/ 
        }),
        /***/ "LD8h": 
        /*!**********************************************************************!*\
          !*** ./src/app/views/payment-creation/payment-creation.component.ts ***!
          \**********************************************************************/
        /*! exports provided: PaymentCreationComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentCreationComponent", function () { return PaymentCreationComponent; });
            /* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
            /* harmony import */ var src_app_entities_payment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/entities/payment */ "C7mK");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
            /* harmony import */ var src_app_services_paymentService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/paymentService */ "jUr9");
            /* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "ofXK");
            function PaymentCreationComponent_div_9_div_1_Template(rf, ctx) {
                if (rf & 1) {
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div");
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, " Naam is verplicht om in te voeren ");
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                }
            }
            function PaymentCreationComponent_div_9_div_2_Template(rf, ctx) {
                if (rf & 1) {
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div");
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, " Naam heeft minimaal 2 karakters nodig ");
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                }
            }
            function PaymentCreationComponent_div_9_Template(rf, ctx) {
                if (rf & 1) {
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 22);
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, PaymentCreationComponent_div_9_div_1_Template, 2, 0, "div", 23);
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](2, PaymentCreationComponent_div_9_div_2_Template, 2, 0, "div", 23);
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                }
                if (rf & 2) {
                    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.from.errors.required);
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.from.errors.minlength);
                }
            }
            function PaymentCreationComponent_div_14_div_1_Template(rf, ctx) {
                if (rf & 1) {
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div");
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, "Dit is geen geldig geldbedrag");
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                }
            }
            function PaymentCreationComponent_div_14_Template(rf, ctx) {
                if (rf & 1) {
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 22);
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, PaymentCreationComponent_div_14_div_1_Template, 2, 0, "div", 23);
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                }
                if (rf & 2) {
                    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
                    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r1.price.errors.min);
                }
            }
            const _c0 = function () { return { standalone: true }; };
            class PaymentCreationComponent {
                constructor(router, paymentService, location) {
                    this.router = router;
                    this.paymentService = paymentService;
                    this.location = location;
                    this.payment = {
                        from: '',
                        category: src_app_entities_payment__WEBPACK_IMPORTED_MODULE_1__["PaymentCategory"].Other,
                        price: 0.01,
                    };
                }
                ngOnInit() {
                    this.paymentForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormGroup"]({
                        from: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](this.payment.from, [
                            _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required,
                            _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].minLength(2),
                        ]),
                        price: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](this.payment.price, [
                            _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].min(0.01)
                        ]),
                    });
                }
                onSubmit() {
                    this.paymentForm.markAllAsTouched();
                    console.log(this.paymentForm.valid);
                    if (this.paymentForm.invalid) {
                        console.log("Invalid form data");
                        return;
                    }
                    this.payment.from = this.from.value;
                    this.payment.price = this.price.value;
                    console.log(this.payment);
                    this.paymentService.addPayment(this.payment).subscribe((result) => {
                        console.log('Added: ');
                        console.log(result);
                        this.router.navigateByUrl('/');
                    }, (error) => {
                        console.log(error);
                    });
                }
                goBack() {
                    this.location.back();
                }
                get from() { return this.paymentForm.get('from'); }
                get price() { return this.paymentForm.get('price'); }
            }
            PaymentCreationComponent.ɵfac = function PaymentCreationComponent_Factory(t) { return new (t || PaymentCreationComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_services_paymentService__WEBPACK_IMPORTED_MODULE_4__["PaymentService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_5__["Location"])); };
            PaymentCreationComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({ type: PaymentCreationComponent, selectors: [["app-payment-creation"]], decls: 41, vars: 9, consts: [[1, "content-container"], [1, "payment-entry-form"], [2, "margin-bottom", "1rem"], ["autocomplete", "off", 3, "formGroup"], [1, "form-group"], ["for", "from"], ["required", "", "minlength", "2", "formControlName", "from", "type", "text", "id", "from", 1, "form-control"], ["class", "alert alert-danger", 4, "ngIf"], ["for", "amount"], ["formControlName", "price", "type", "number", "min", "0.01", "id", "amount", 1, "form-control"], ["for", "categorie"], ["type", "text", "id", "categorie", 1, "form-control", 3, "ngModel", "ngModelOptions", "ngModelChange"], ["value", "Bioscoop"], ["value", "Restaurant"], ["value", "Karten"], ["value", "Museum"], ["value", "Voorgeschoten"], ["value", "Overig"], ["for", "description"], ["rows", "6", 1, "form-control", 2, "resize", "vertical", "min-height", "15rem", 3, "ngModel", "ngModelOptions", "ngModelChange"], [1, "entry-actions"], [1, "btn", "btn-primary", 3, "click"], [1, "alert", "alert-danger"], [4, "ngIf"]], template: function PaymentCreationComponent_Template(rf, ctx) {
                    if (rf & 1) {
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 0);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "div", 1);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](2, "h1", 2);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](3, "Betaling toevoegen");
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](4, "form", 3);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "div", 4);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](6, "label", 5);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](7, "Van");
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](8, "input", 6);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](9, PaymentCreationComponent_div_9_Template, 3, 2, "div", 7);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](10, "div", 4);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](11, "label", 8);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](12, "Prijs");
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](13, "input", 9);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](14, PaymentCreationComponent_div_14_Template, 2, 1, "div", 7);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](15, "div", 4);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](16, "label", 10);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](17, "Waarvoor");
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](18, "select", 11);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("ngModelChange", function PaymentCreationComponent_Template_select_ngModelChange_18_listener($event) { return ctx.payment.category = $event; });
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](19, "option", 12);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](20, "Bioscoop");
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](21, "option", 13);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](22, "Restaurant");
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](23, "option", 14);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](24, "Karten");
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](25, "option", 15);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](26, "Museum");
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](27, "option", 16);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](28, "Voorgeschoten");
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](29, "option", 17);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](30, "Overig");
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](31, "div", 4);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](32, "label", 18);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](33, "Beschrijving");
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](34, "textarea", 19);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("ngModelChange", function PaymentCreationComponent_Template_textarea_ngModelChange_34_listener($event) { return ctx.payment.description = $event; });
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](35, "div", 20);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](36, "div", 21);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function PaymentCreationComponent_Template_div_click_36_listener() { return ctx.goBack(); });
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](37, "Cancel");
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](38, "div", 21);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function PaymentCreationComponent_Template_div_click_38_listener() { return ctx.onSubmit(); });
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](39, "span");
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](40, "Betaling toevoegen");
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
                    }
                    if (rf & 2) {
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](4);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("formGroup", ctx.paymentForm);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](5);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.from.invalid && (ctx.from.dirty || ctx.from.touched));
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](5);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.price.invalid && (ctx.price.dirty || ctx.price.touched));
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](4);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngModel", ctx.payment.category)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpureFunction0"](7, _c0));
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](16);
                        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngModel", ctx.payment.description)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpureFunction0"](8, _c0));
                    }
                }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["MinLengthValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControlName"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NumberValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["SelectControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_forms_forms_x"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwYXltZW50LWNyZWF0aW9uLmNvbXBvbmVudC5zY3NzIn0= */"] });
            /***/ 
        }),
        /***/ "Sy1n": 
        /*!**********************************!*\
          !*** ./src/app/app.component.ts ***!
          \**********************************/
        /*! exports provided: AppComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function () { return AppComponent; });
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
            /* harmony import */ var _services_registerService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./services/registerService */ "3STb");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
            /* harmony import */ var _services_paymentService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/paymentService */ "jUr9");
            /* harmony import */ var _services_peopleService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./services/peopleService */ "T0Np");
            /* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "1kSV");
            class AppComponent {
                constructor(registerService, route, paymentService, peopleService) {
                    this.registerService = registerService;
                    this.route = route;
                    this.paymentService = paymentService;
                    this.peopleService = peopleService;
                    this.title = 'angular-front-end';
                    registerService.createRegister().subscribe((result) => {
                        console.log(result);
                        registerService.registerId = result;
                        console.log(registerService.registerId);
                        this.paymentService.fetchPayments();
                        this.peopleService.fetchPool();
                    });
                }
            }
            AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_registerService__WEBPACK_IMPORTED_MODULE_1__["RegisterService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_paymentService__WEBPACK_IMPORTED_MODULE_3__["PaymentService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_peopleService__WEBPACK_IMPORTED_MODULE_4__["PeopleService"])); };
            AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 12, vars: 0, consts: [[1, "page-layout"], [1, "navbar", "navbar-expand-md", "navbar-light", "navigation"], ["routerLink", "/", 1, "navbar-brand", "navigation-head"], ["src", "/assets/logo.svg", 1, "navigation-logo"], [1, "navigation-title"]], template: function AppComponent_Template(rf, ctx) {
                    if (rf & 1) {
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "header");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "nav", 1);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "a", 2);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 3);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h1", 4);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Payback");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "main");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "router-outlet");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "footer");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "small");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Ian van de Poll - 2021");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                    }
                }, directives: [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbNavbar"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkWithHref"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterOutlet"]], styles: [".navigation[_ngcontent-%COMP%] {\n  background: #455a64;\n  justify-content: center;\n}\n\n.navigation-head[_ngcontent-%COMP%] {\n  display: flex;\n  position: relative;\n  color: inherit;\n  text-decoration: none;\n  align-items: center;\n  justify-content: center;\n}\n\n.navigation-logo[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  height: 3rem;\n  margin-right: 1.5rem;\n}\n\n.navigation-title[_ngcontent-%COMP%] {\n  color: white;\n  font-size: 26px;\n  font-weight: 700;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcZGlzdFxcYW5ndWxhci1mcm9udC1lbmRcXGFwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLG1CQUFBO0VBRUEsdUJBQUE7QUFGRjs7QUFLQTtFQUNFLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUFGRjs7QUFLQTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtBQUZGOztBQUtBO0VBQ0UsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUZGIiwiZmlsZSI6ImFwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkB1c2UgXCIuLi92YXJpYWJsZXNcIiBhcyB2O1xuXG4ubmF2aWdhdGlvbiB7XG4gIGJhY2tncm91bmQ6ICM0NTVhNjQ7XG5cbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5uYXZpZ2F0aW9uLWhlYWQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGNvbG9yOiBpbmhlcml0O1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4ubmF2aWdhdGlvbi1sb2dvIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgaGVpZ2h0OiAzcmVtO1xuICBtYXJnaW4tcmlnaHQ6IDEuNXJlbTtcbn1cblxuLm5hdmlnYXRpb24tdGl0bGUge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cbiJdfQ== */"] });
            /***/ 
        }),
        /***/ "T0Np": 
        /*!*******************************************!*\
          !*** ./src/app/services/peopleService.ts ***!
          \*******************************************/
        /*! exports provided: PeopleService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PeopleService", function () { return PeopleService; });
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "qCKp");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
            /* harmony import */ var _registerService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./registerService */ "3STb");
            class PeopleService {
                constructor(http, registerService) {
                    this.http = http;
                    this.registerService = registerService;
                    this.poolUrl = '/api/pool';
                    this.people = [];
                    this.httpOptions = {
                        headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]({ 'Content-Type': 'application/json' }),
                    };
                }
                fetchPool() {
                    const url = `${this.poolUrl}/?payback=${this.registerService.registerId}`;
                    this.http.get(url).subscribe((x) => {
                        this.people = x;
                    });
                }
                getPeople() {
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(this.people);
                }
                getPerson(id) {
                    const heroIndex = this.people.findIndex((person) => {
                        return person.id === id;
                    });
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(this.people[heroIndex]);
                }
                addPerson(person) {
                    const url = `${this.poolUrl}?payback=${this.registerService.registerId}`;
                    return this.http
                        .post(url, person, this.httpOptions)
                        .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])((x) => this.people.push(x)));
                }
                deletePerson(person) {
                    const url = `${this.poolUrl}/${person.id}?payback=${this.registerService.registerId}`;
                    this.http.delete(url, this.httpOptions).subscribe((res) => {
                        this.people.splice(this.people.findIndex((x) => x.id === person.id), 1);
                    }, (err) => {
                        this.people.splice(this.people.findIndex((x) => x.id === person.id), 1);
                    });
                }
            }
            PeopleService.ɵfac = function PeopleService_Factory(t) { return new (t || PeopleService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_registerService__WEBPACK_IMPORTED_MODULE_4__["RegisterService"])); };
            PeopleService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({ token: PeopleService, factory: PeopleService.ɵfac, providedIn: 'root' });
            /***/ 
        }),
        /***/ "ZAI4": 
        /*!*******************************!*\
          !*** ./src/app/app.module.ts ***!
          \*******************************/
        /*! exports provided: AppModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function () { return AppModule; });
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
            /* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
            /* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "Sy1n");
            /* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "1kSV");
            /* harmony import */ var _components_payment_overview_payment_overview_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/payment-overview/payment-overview.component */ "p7pC");
            /* harmony import */ var _components_payment_payment_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/payment/payment.component */ "joBQ");
            /* harmony import */ var _components_people_overview_people_overview_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/people-overview/people-overview.component */ "9hoZ");
            /* harmony import */ var _components_person_person_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/person/person.component */ "2Rw8");
            /* harmony import */ var _components_transaction_overview_transaction_overview_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/transaction-overview/transaction-overview.component */ "A0Hx");
            /* harmony import */ var _components_transaction_transaction_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/transaction/transaction.component */ "BYgT");
            /* harmony import */ var _views_home_view_home_view_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./views/home-view/home-view.component */ "vWU6");
            /* harmony import */ var _views_person_creation_person_creation_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./views/person-creation/person-creation.component */ "JsTZ");
            /* harmony import */ var _views_payment_creation_payment_creation_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./views/payment-creation/payment-creation.component */ "LD8h");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
            /* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/core */ "fXoL");
            class AppModule {
            }
            AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]] });
            AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [], imports: [[
                        _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                        _app_routing_module__WEBPACK_IMPORTED_MODULE_1__["AppRoutingModule"],
                        _angular_common_http__WEBPACK_IMPORTED_MODULE_13__["HttpClientModule"],
                        _angular_forms__WEBPACK_IMPORTED_MODULE_14__["FormsModule"],
                        _angular_forms__WEBPACK_IMPORTED_MODULE_14__["ReactiveFormsModule"],
                        _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModule"],
                    ]] });
            (function () {
                (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
                        _views_home_view_home_view_component__WEBPACK_IMPORTED_MODULE_10__["HomeViewComponent"],
                        _components_payment_overview_payment_overview_component__WEBPACK_IMPORTED_MODULE_4__["PaymentOverviewComponent"],
                        _components_payment_payment_component__WEBPACK_IMPORTED_MODULE_5__["PaymentComponent"],
                        _components_people_overview_people_overview_component__WEBPACK_IMPORTED_MODULE_6__["PeopleOverviewComponent"],
                        _components_person_person_component__WEBPACK_IMPORTED_MODULE_7__["PersonComponent"],
                        _views_person_creation_person_creation_component__WEBPACK_IMPORTED_MODULE_11__["PersonCreationComponent"],
                        _views_payment_creation_payment_creation_component__WEBPACK_IMPORTED_MODULE_12__["PaymentCreationComponent"],
                        _components_transaction_overview_transaction_overview_component__WEBPACK_IMPORTED_MODULE_8__["TransactionOverviewComponent"],
                        _components_transaction_transaction_component__WEBPACK_IMPORTED_MODULE_9__["TransactionComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                        _app_routing_module__WEBPACK_IMPORTED_MODULE_1__["AppRoutingModule"],
                        _angular_common_http__WEBPACK_IMPORTED_MODULE_13__["HttpClientModule"],
                        _angular_forms__WEBPACK_IMPORTED_MODULE_14__["FormsModule"],
                        _angular_forms__WEBPACK_IMPORTED_MODULE_14__["ReactiveFormsModule"],
                        _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModule"]] });
            })();
            /***/ 
        }),
        /***/ "jUr9": 
        /*!********************************************!*\
          !*** ./src/app/services/paymentService.ts ***!
          \********************************************/
        /*! exports provided: PaymentService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentService", function () { return PaymentService; });
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "qCKp");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
            /* harmony import */ var _registerService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./registerService */ "3STb");
            class PaymentService {
                constructor(http, registerService) {
                    this.http = http;
                    this.registerService = registerService;
                    this.paymentsUrl = '/api/payments';
                    this.payments = [];
                    this.httpOptions = {
                        headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]({ 'Content-Type': 'application/json' }),
                    };
                }
                fetchPayments() {
                    const url = `${this.paymentsUrl}/?payback=${this.registerService.registerId}`;
                    this.http.get(url).subscribe((x) => {
                        this.payments = x;
                    });
                }
                getPayments() {
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(this.payments);
                }
                getPayment(id) {
                    const heroIndex = this.payments.findIndex((payment) => {
                        return payment.id === id;
                    });
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(this.payments[heroIndex]);
                }
                addPayment(payment) {
                    const url = `${this.paymentsUrl}/?payback=${this.registerService.registerId}`;
                    return this.http
                        .post(url, payment, this.httpOptions)
                        .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])((x) => this.payments.push(x)));
                }
                deletePayment(payment) {
                    const url = `${this.paymentsUrl}/${payment.id}?payback=${this.registerService.registerId}`;
                    console.log(url);
                    return this.http.delete(url, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])((x) => (this.payments.splice(this.payments.findIndex((x) => x.id === payment.id), 1))), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])((x) => (this.payments.splice(this.payments.findIndex((x) => x.id === payment.id), 1))));
                }
            }
            PaymentService.ɵfac = function PaymentService_Factory(t) { return new (t || PaymentService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_registerService__WEBPACK_IMPORTED_MODULE_4__["RegisterService"])); };
            PaymentService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({ token: PaymentService, factory: PaymentService.ɵfac, providedIn: 'root' });
            /***/ 
        }),
        /***/ "joBQ": 
        /*!*********************************************************!*\
          !*** ./src/app/components/payment/payment.component.ts ***!
          \*********************************************************/
        /*! exports provided: PaymentComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentComponent", function () { return PaymentComponent; });
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
            /* harmony import */ var _services_paymentService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/paymentService */ "jUr9");
            /* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
            function PaymentComponent_p_26_Template(rf, ctx) {
                if (rf & 1) {
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                }
                if (rf & 2) {
                    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r0.payment.description);
                }
            }
            function PaymentComponent_p_27_Template(rf, ctx) {
                if (rf & 1) {
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Geen beschrijving aanwezig");
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                }
            }
            class PaymentComponent {
                constructor(paymentService) {
                    this.paymentService = paymentService;
                }
                ngOnInit() { }
                deletePayment() {
                    this.paymentService.deletePayment(this.payment).subscribe(() => {
                        console.log("Deleting");
                    });
                }
            }
            PaymentComponent.ɵfac = function PaymentComponent_Factory(t) { return new (t || PaymentComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_paymentService__WEBPACK_IMPORTED_MODULE_1__["PaymentService"])); };
            PaymentComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PaymentComponent, selectors: [["app-payment"]], inputs: { payment: "payment" }, decls: 31, vars: 5, consts: [[1, "card"], [1, "card-body"], [1, "payment-name"], [1, "payment-general-wrapper"], [1, "payment-general-price"], [1, "payment-general-category"], [1, "payment-description"], [4, "ngIf"], [1, "payment-delete-body"], [1, "material-icons", "payment-delete-remove", 3, "click"]], template: function PaymentComponent_Template(rf, ctx) {
                    if (rf & 1) {
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "small");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "i");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Door:");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h2");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 3);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 4);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "small");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "i");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Kosten: ");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "span");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 5);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "small");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "i");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Waarvoor?");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "span");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "hr");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 6);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "small");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "i");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Beschrijving:");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](26, PaymentComponent_p_26_Template, 2, 1, "p", 7);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, PaymentComponent_p_27_Template, 2, 0, "p", 7);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 8);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "i", 9);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PaymentComponent_Template_i_click_29_listener() { return ctx.deletePayment(); });
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, " close ");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                    }
                    if (rf & 2) {
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.payment.from);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.payment.price);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.payment.category);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.payment.description);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.payment.description);
                    }
                }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"]], styles: [".payment-card[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  flex-direction: column;\n  word-wrap: break-word;\n  height: 100%;\n  border: 2px solid #455a64;\n  border-radius: 0.2rem;\n}\n\n.card-body[_ngcontent-%COMP%]   small[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  color: #a0a0a0;\n  margin-bottom: 0.75rem;\n}\n\n.payment-container[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  text-decoration: none;\n  color: #000000;\n  padding: 0.5rem 0.5rem;\n  margin: 0.5rem 0.5rem;\n}\n\n.payment-general-wrapper[_ngcontent-%COMP%] {\n  display: flex;\n  position: relative;\n}\n\n.payment-general-price[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  width: 50%;\n}\n\n.payment-general-category[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  width: 50%;\n}\n\n.payment-name[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  color: inherit;\n  margin-top: 0.5rem;\n  margin-bottom: 0.5rem;\n}\n\n.payment-description[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  margin: 0.5rem 0;\n}\n\n.payment-delete-body[_ngcontent-%COMP%] {\n  display: block;\n  position: absolute;\n  margin: 0;\n  top: 0;\n  right: 0;\n  z-index: 1;\n}\n\n.payment-delete-remove[_ngcontent-%COMP%] {\n  cursor: pointer;\n  padding: 0.5rem 0.5rem;\n}\n\n.payment-delete-remove[_ngcontent-%COMP%]:hover {\n  color: #b31616;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxkaXN0XFxhbmd1bGFyLWZyb250LWVuZFxccGF5bWVudC5jb21wb25lbnQuc2NzcyIsIi4uXFwuLlxcLi5cXC4uXFwuLlxcdmFyaWFibGVzLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxjQUFBO0VBQ0Esa0JBQUE7RUFFQSxzQkFBQTtFQUNBLHFCQUFBO0VBRUEsWUFBQTtFQUVBLHlCQUFBO0VBQ0EscUJBQUE7QUFKRjs7QUFPQTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUVBLGNBQUE7RUFFQSxzQkFBQTtBQU5GOztBQVNBO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBRUEscUJBQUE7RUFDQSxjQ3JCcUI7RUR1QnJCLHNCQUFBO0VBQ0EscUJBQUE7QUFSRjs7QUFXQTtFQUNFLGFBQUE7RUFDQSxrQkFBQTtBQVJGOztBQVdBO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBRUEsVUFBQTtBQVRGOztBQVlBO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBRUEsVUFBQTtBQVZGOztBQWFBO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBRUEsY0FBQTtFQUVBLGtCQUFBO0VBQ0EscUJBQUE7QUFaRjs7QUFlQTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUVBLGdCQUFBO0FBYkY7O0FBZ0JBO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBRUEsU0FBQTtFQUVBLE1BQUE7RUFDQSxRQUFBO0VBRUEsVUFBQTtBQWhCRjs7QUFtQkE7RUFDRSxlQUFBO0VBRUEsc0JBQUE7QUFqQkY7O0FBbUJFO0VBQ0UsY0FBQTtBQWpCSiIsImZpbGUiOiJwYXltZW50LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQHVzZSAnLi4vLi4vLi4vdmFyaWFibGVzLnNjc3MnIGFzIHY7XG5cbi5wYXltZW50LWNhcmQge1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIHdvcmQtd3JhcDogYnJlYWstd29yZDtcblxuICBoZWlnaHQ6IDEwMCU7XG5cbiAgYm9yZGVyOiAycHggc29saWQgdi4kcHJpbWFyeS1jb2xvcjtcbiAgYm9yZGVyLXJhZGl1czogMC4ycmVtO1xufVxuXG4uY2FyZC1ib2R5IHNtYWxsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICBjb2xvcjogI2EwYTBhMDtcblxuICBtYXJnaW4tYm90dG9tOiAwLjc1cmVtO1xufVxuXG4ucGF5bWVudC1jb250YWluZXIge1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgY29sb3I6IHYuJHNlY29uZGFyeS10ZXh0LWNvbG9yO1xuXG4gIHBhZGRpbmc6IDAuNXJlbSAwLjVyZW07XG4gIG1hcmdpbjogMC41cmVtIDAuNXJlbTtcbn1cblxuLnBheW1lbnQtZ2VuZXJhbC13cmFwcGVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4ucGF5bWVudC1nZW5lcmFsLXByaWNlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICB3aWR0aDogNTAlO1xufVxuXG4ucGF5bWVudC1nZW5lcmFsLWNhdGVnb3J5IHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICB3aWR0aDogNTAlO1xufVxuXG4ucGF5bWVudC1uYW1lIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICBjb2xvcjogaW5oZXJpdDtcblxuICBtYXJnaW4tdG9wOiAwLjVyZW07XG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcbn1cblxuLnBheW1lbnQtZGVzY3JpcHRpb24ge1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gIG1hcmdpbjogMC41cmVtIDA7XG59XG5cbi5wYXltZW50LWRlbGV0ZS1ib2R5IHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcblxuICBtYXJnaW46IDA7XG5cbiAgdG9wOiAwO1xuICByaWdodDogMDtcblxuICB6LWluZGV4OiAxO1xufVxuXG4ucGF5bWVudC1kZWxldGUtcmVtb3ZlIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xuXG4gIHBhZGRpbmc6IDAuNXJlbSAwLjVyZW07XG5cbiAgJjpob3ZlciB7XG4gICAgY29sb3I6ICNiMzE2MTY7XG4gIH1cbn0iLCIkcHJpbWFyeS1jb2xvcjogIzQ1NWE2NDtcbiRwcmltYXJ5LWNvbG9yLWxpZ2h0OiAjNzE4NzkyO1xuJHByaW1hcnktY29sb3ItZGFyazogIzFjMzEzYTtcbiRwcmltYXJ5LXRleHQtY29sb3I6ICNmZmZmZmY7XG5cbiRzZWNvbmRhcnktY29sb3I6ICNmZmEwMDA7XG4kc2Vjb25kYXJ5LWNvbG9yLWxpZ2h0OiAjZmZkMTQ5O1xuJHNlY29uZGFyeS1jb2xvci1kYXJrOiAjYzY3MTAwO1xuJHNlY29uZGFyeS10ZXh0LWNvbG9yOiAjMDAwMDAwO1xuXG4kdGV4dC1zaXplOiAxNnB4O1xuJGJvbGQtc2l6ZTogMjBweDtcbiR0aXRsZS1zaXplOiAyNnB4O1xuXG4kYnJlYWtwb2ludHM6ICg1NzZweCwgNzY4cHgsIDk5MnB4LCAxMjAwcHgsIDE0MDBweCk7XG4kYnJlYWtzaXplczogKDU0MHB4LCA3MjBweCwgOTYwcHgsIDExNDBweCwgMTMyMHB4KTtcblxuJGZvbnQtZmFtaWx5OiAnUm9ib3RvJywgc2Fucy1zZXJpZjtcbiRmb250LXdlaWdodDogNDAwO1xuXG4iXX0= */"] });
            /***/ 
        }),
        /***/ "p7pC": 
        /*!***************************************************************************!*\
          !*** ./src/app/components/payment-overview/payment-overview.component.ts ***!
          \***************************************************************************/
        /*! exports provided: PaymentOverviewComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentOverviewComponent", function () { return PaymentOverviewComponent; });
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
            /* harmony import */ var src_app_services_paymentService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/paymentService */ "jUr9");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
            /* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");
            /* harmony import */ var _payment_payment_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../payment/payment.component */ "joBQ");
            function PaymentOverviewComponent_ol_7_li_1_Template(rf, ctx) {
                if (rf & 1) {
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li", 8);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-payment", 9);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                }
                if (rf & 2) {
                    const payment_r3 = ctx.$implicit;
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("payment", payment_r3);
                }
            }
            function PaymentOverviewComponent_ol_7_Template(rf, ctx) {
                if (rf & 1) {
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ol", 6);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, PaymentOverviewComponent_ol_7_li_1_Template, 2, 1, "li", 7);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                }
                if (rf & 2) {
                    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r0.payments);
                }
            }
            function PaymentOverviewComponent_div_8_Template(rf, ctx) {
                if (rf & 1) {
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 10);
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span");
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Er zijn nog geen betalingen ingevoerd");
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                }
            }
            class PaymentOverviewComponent {
                constructor(paymentService) {
                    this.paymentService = paymentService;
                    this.payments = [];
                }
                ngOnInit() {
                    this.paymentService.getPayments().subscribe((x) => {
                        this.payments = x;
                    });
                }
            }
            PaymentOverviewComponent.ɵfac = function PaymentOverviewComponent_Factory(t) { return new (t || PaymentOverviewComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_paymentService__WEBPACK_IMPORTED_MODULE_1__["PaymentService"])); };
            PaymentOverviewComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PaymentOverviewComponent, selectors: [["app-payment-overview"]], decls: 9, vars: 2, consts: [[1, "content-container", "payments-wrapper"], [1, "payments-header"], ["routerLink", "/payment", 1, "material-icons", "payments", "payments-header-add"], [1, "payments-container"], ["class", "payments-list", 4, "ngIf"], ["class", "payments-empty", 4, "ngIf"], [1, "payments-list"], ["class", "payments-item", 4, "ngFor", "ngForOf"], [1, "payments-item"], [3, "payment"], [1, "payments-empty"]], template: function PaymentOverviewComponent_Template(rf, ctx) {
                    if (rf & 1) {
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Uitgaven");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "a", 2);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " add_box ");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 3);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, PaymentOverviewComponent_ol_7_Template, 2, 1, "ol", 4);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, PaymentOverviewComponent_div_8_Template, 3, 0, "div", 5);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
                    }
                    if (rf & 2) {
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.payments.length !== 0);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.payments.length === 0);
                    }
                }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"], _payment_payment_component__WEBPACK_IMPORTED_MODULE_4__["PaymentComponent"]], styles: [".payments-wrapper[_ngcontent-%COMP%] {\n  margin-top: 2rem;\n  min-height: 15rem;\n}\n\n.payments-container[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  width: 100%;\n  margin-bottom: 1.5rem;\n}\n\n.payments-header[_ngcontent-%COMP%] {\n  display: flex;\n  position: relative;\n  justify-content: space-between;\n  align-items: center;\n}\n\n.payments-header-add[_ngcontent-%COMP%] {\n  color: #000000;\n  padding: 0.4rem;\n  text-decoration: none;\n}\n\n.payments-list[_ngcontent-%COMP%] {\n  display: grid;\n  position: relative;\n  flex-wrap: wrap;\n  grid-template-columns: repeat(4, 1fr);\n  gap: 1rem 0.75rem;\n  padding: 0;\n  margin-top: 2rem;\n  margin-bottom: 1rem;\n}\n\n.payments-item[_ngcontent-%COMP%] {\n  margin: 0;\n  padding: 0;\n  flex: 1 0 25%;\n  list-style-type: none;\n}\n\n.payments-empty[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  font-size: 20px;\n  margin-top: 1.5rem;\n  margin-bottom: 1.5rem;\n}\n\n@media screen and (max-width: 992px) {\n  .payments-list[_ngcontent-%COMP%] {\n    grid-template-columns: repeat(1, 1fr);\n    gap: 0.5rem 0.5rem;\n  }\n}\n\n@media screen and (min-width: 992px) {\n  .payments-list[_ngcontent-%COMP%] {\n    grid-template-columns: repeat(3, 1fr);\n  }\n}\n\n@media screen and (min-width: 1200px) {\n  .payments-list[_ngcontent-%COMP%] {\n    grid-template-columns: repeat(4, 1fr);\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxkaXN0XFxhbmd1bGFyLWZyb250LWVuZFxccGF5bWVudC1vdmVydmlldy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFBO0VBRUEsaUJBQUE7QUFBRjs7QUFHQTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUVBLFdBQUE7RUFFQSxxQkFBQTtBQUZGOztBQUtBO0VBQ0UsYUFBQTtFQUNBLGtCQUFBO0VBRUEsOEJBQUE7RUFDQSxtQkFBQTtBQUhGOztBQU1BO0VBQ0UsY0FBQTtFQUVBLGVBQUE7RUFFQSxxQkFBQTtBQUxGOztBQVFBO0VBQ0UsYUFBQTtFQUNBLGtCQUFBO0VBRUEsZUFBQTtFQUVBLHFDQUFBO0VBQ0EsaUJBQUE7RUFFQSxVQUFBO0VBRUEsZ0JBQUE7RUFDQSxtQkFBQTtBQVRGOztBQVlBO0VBQ0UsU0FBQTtFQUNBLFVBQUE7RUFFQSxhQUFBO0VBQ0EscUJBQUE7QUFWRjs7QUFhQTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUVBLGVBQUE7RUFFQSxrQkFBQTtFQUNBLHFCQUFBO0FBWkY7O0FBZUE7RUFDRTtJQUNFLHFDQUFBO0lBQ0Esa0JBQUE7RUFaRjtBQUNGOztBQWVBO0VBQ0U7SUFDRSxxQ0FBQTtFQWJGO0FBQ0Y7O0FBZ0JBO0VBQ0U7SUFDRSxxQ0FBQTtFQWRGO0FBQ0YiLCJmaWxlIjoicGF5bWVudC1vdmVydmlldy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wYXltZW50cy13cmFwcGVyIHtcbiAgbWFyZ2luLXRvcDogMnJlbTtcblxuICBtaW4taGVpZ2h0OiAxNXJlbTtcbn1cblxuLnBheW1lbnRzLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgd2lkdGg6IDEwMCU7XG5cbiAgbWFyZ2luLWJvdHRvbTogMS41cmVtO1xufVxuXG4ucGF5bWVudHMtaGVhZGVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuLnBheW1lbnRzLWhlYWRlci1hZGQge1xuICBjb2xvcjogIzAwMDAwMDtcblxuICBwYWRkaW5nOiAwLjRyZW07XG5cbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuXG4ucGF5bWVudHMtbGlzdCB7XG4gIGRpc3BsYXk6IGdyaWQ7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICBmbGV4LXdyYXA6IHdyYXA7XG5cbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoNCwgMWZyKTtcbiAgZ2FwOiAxcmVtIDAuNzVyZW07XG5cbiAgcGFkZGluZzogMDtcblxuICBtYXJnaW4tdG9wOiAycmVtO1xuICBtYXJnaW4tYm90dG9tOiAxcmVtO1xufVxuXG4ucGF5bWVudHMtaXRlbSB7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMDtcblxuICBmbGV4OiAxIDAgMjUlO1xuICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG59XG5cbi5wYXltZW50cy1lbXB0eSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgZm9udC1zaXplOiAyMHB4O1xuXG4gIG1hcmdpbi10b3A6IDEuNXJlbTtcbiAgbWFyZ2luLWJvdHRvbTogMS41cmVtO1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTJweCkge1xuICAucGF5bWVudHMtbGlzdCB7XG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMSwgMWZyKTtcbiAgICBnYXA6IDAuNXJlbSAwLjVyZW07XG4gIH1cbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDogOTkycHgpIHtcbiAgLnBheW1lbnRzLWxpc3Qge1xuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDMsIDFmcik7XG4gIH1cbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDogMTIwMHB4KSB7XG4gIC5wYXltZW50cy1saXN0IHtcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCg0LCAxZnIpO1xuICB9XG59XG4iXX0= */"] });
            /***/ 
        }),
        /***/ "vWU6": 
        /*!********************************************************!*\
          !*** ./src/app/views/home-view/home-view.component.ts ***!
          \********************************************************/
        /*! exports provided: HomeViewComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeViewComponent", function () { return HomeViewComponent; });
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
            /* harmony import */ var _components_people_overview_people_overview_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/people-overview/people-overview.component */ "9hoZ");
            /* harmony import */ var _components_payment_overview_payment_overview_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/payment-overview/payment-overview.component */ "p7pC");
            /* harmony import */ var _components_transaction_overview_transaction_overview_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/transaction-overview/transaction-overview.component */ "A0Hx");
            class HomeViewComponent {
                constructor() { }
                ngOnInit() {
                }
            }
            HomeViewComponent.ɵfac = function HomeViewComponent_Factory(t) { return new (t || HomeViewComponent)(); };
            HomeViewComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeViewComponent, selectors: [["app-home-view"]], decls: 3, vars: 0, template: function HomeViewComponent_Template(rf, ctx) {
                    if (rf & 1) {
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-people-overview");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-payment-overview");
                        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "app-transaction-overview");
                    }
                }, directives: [_components_people_overview_people_overview_component__WEBPACK_IMPORTED_MODULE_1__["PeopleOverviewComponent"], _components_payment_overview_payment_overview_component__WEBPACK_IMPORTED_MODULE_2__["PaymentOverviewComponent"], _components_transaction_overview_transaction_overview_component__WEBPACK_IMPORTED_MODULE_3__["TransactionOverviewComponent"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJob21lLXZpZXcuY29tcG9uZW50LnNjc3MifQ== */"] });
            /***/ 
        }),
        /***/ "vY5A": 
        /*!***************************************!*\
          !*** ./src/app/app-routing.module.ts ***!
          \***************************************/
        /*! exports provided: AppRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () { return AppRoutingModule; });
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
            /* harmony import */ var _views_home_view_home_view_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./views/home-view/home-view.component */ "vWU6");
            /* harmony import */ var _views_person_creation_person_creation_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./views/person-creation/person-creation.component */ "JsTZ");
            /* harmony import */ var _views_payment_creation_payment_creation_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./views/payment-creation/payment-creation.component */ "LD8h");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "fXoL");
            const routes = [
                { path: "", component: _views_home_view_home_view_component__WEBPACK_IMPORTED_MODULE_1__["HomeViewComponent"] },
                { path: "people", component: _views_person_creation_person_creation_component__WEBPACK_IMPORTED_MODULE_2__["PersonCreationComponent"] },
                { path: "payment", component: _views_payment_creation_payment_creation_component__WEBPACK_IMPORTED_MODULE_3__["PaymentCreationComponent"] }
            ];
            class AppRoutingModule {
            }
            AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
            AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] });
            (function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] }); })();
            /***/ 
        }),
        /***/ "zUnb": 
        /*!*********************!*\
          !*** ./src/main.ts ***!
          \*********************/
        /*! no exports provided */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
            /* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
            /* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");
            if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
            }
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
                .catch(err => console.error(err));
            /***/ 
        }),
        /***/ "zn8P": 
        /*!******************************************************!*\
          !*** ./$$_lazy_route_resource lazy namespace object ***!
          \******************************************************/
        /*! no static exports found */
        /***/ (function (module, exports) {
            function webpackEmptyAsyncContext(req) {
                // Here Promise.resolve().then() is used instead of new Promise() to prevent
                // uncaught exception popping up in devtools
                return Promise.resolve().then(function () {
                    var e = new Error("Cannot find module '" + req + "'");
                    e.code = 'MODULE_NOT_FOUND';
                    throw e;
                });
            }
            webpackEmptyAsyncContext.keys = function () { return []; };
            webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
            module.exports = webpackEmptyAsyncContext;
            webpackEmptyAsyncContext.id = "zn8P";
            /***/ 
        })
    }, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main.js.map
