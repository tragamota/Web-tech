"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.paymentSchema = exports.PaymentCategory = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
var PaymentCategory;
(function (PaymentCategory) {
    PaymentCategory["Bioscoop"] = "Bioscoop";
    PaymentCategory["Restaurant"] = "Restaurant";
    PaymentCategory["Karting"] = "Karten";
    PaymentCategory["Museum"] = "Museum";
    PaymentCategory["AdvancedPayment"] = "Voorgeschoten";
    PaymentCategory["Other"] = "Overig";
})(PaymentCategory = exports.PaymentCategory || (exports.PaymentCategory = {}));
exports.paymentSchema = mongoose_1.default.model("payment", new mongoose_1.default.Schema({
    id: String,
    from: String,
    price: Number,
    category: String,
    description: String,
    registerId: String,
}));
