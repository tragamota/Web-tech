"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.personSchema = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
exports.personSchema = mongoose_1.default.model("person", new mongoose_1.default.Schema({
    id: String,
    name: String,
    registerId: String,
}));
