"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const compression_1 = __importDefault(require("compression"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const mongoose_1 = __importDefault(require("mongoose"));
const paymentRouter_1 = require("./router/paymentRouter");
const peopleRouter_1 = require("./router/peopleRouter");
const registerRouter_1 = require("./router/registerRouter");
const TransactionRouter_1 = require("./router/TransactionRouter");
mongoose_1.default.connect('mongodb://localhost:27017/payback', { useNewUrlParser: true, useUnifiedTopology: true });
const App = express_1.default();
App.use(compression_1.default());
App.use(cors_1.default());
App.use(body_parser_1.default.json());
App.use(body_parser_1.default.urlencoded({ extended: true, }));
App.use("/", express_1.default.static("./public"));
App.use("/api", paymentRouter_1.PaymentRoutes);
App.use("/api", peopleRouter_1.PeopleRoutes);
App.use("/api", registerRouter_1.RegisterRoutes);
App.use("/api", TransactionRouter_1.TransactionRoutes);
App.use("*", (req, res, next) => {
    const error = {
        message: "Endpoint does not exist",
        code: 404,
        Datetime: new Date().toISOString(),
    };
    next(error);
});
App.use((error, req, res, next) => {
    res
        .status(error.code)
        .json({ errorMessage: error.message, errorTime: error.Datetime })
        .end();
});
// console.log(`Database connected?: ${ CashRegisterDatabase.getInstance().createConnections() }`)
var WebServer = App.listen(3000, "0.0.0.0", () => {
    console.log(`Server started on ${3000} `);
});
