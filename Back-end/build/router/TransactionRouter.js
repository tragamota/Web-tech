"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionRoutes = void 0;
const express_1 = require("express");
const payment_1 = require("../models/payment");
const Person_1 = require("../models/Person");
const router = express_1.Router();
exports.TransactionRoutes = router;
router.get("/transactions", async (req, res) => {
    const registerId = req.query.payback;
    if (!registerId) {
        res.status(404).end();
        return;
    }
    let payments = [];
    let pool = [];
    let transactions = [];
    await payment_1.paymentSchema
        .find({}, "id from price category description")
        .where("registerId")
        .equals(registerId)
        .exec((err, result) => {
        if (err) {
            console.log(err);
        }
        let paymentsLocal = result;
        Person_1.personSchema
            .find({}, "name")
            .where("registerId")
            .equals(registerId)
            .exec((err, result) => {
            if (err) {
                console.log(err);
            }
            let people = result;
            // algorithm
            people.forEach((element) => {
                pool.push(element.toObject());
            });
            paymentsLocal.forEach((element) => {
                payments.push(element.toObject());
            });
            console.log(pool);
            console.log(payments);
            payments.forEach(element => {
                const transactionPool = pool.filter((x) => x.name !== element.from);
                transactionPool.forEach(element2 => {
                    transactions.push({ from: element2.name, to: element.from, price: element.price / transactionPool.length });
                });
            });
            res.json(transactions).end();
        });
    });
});
