"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentRoutes = void 0;
const express_1 = require("express");
const payment_1 = require("../models/payment");
const router = express_1.Router();
exports.PaymentRoutes = router;
router.get("/payments", (req, res) => {
    const registerId = req.query.payback;
    if (!registerId) {
        res.status(404).end();
        return;
    }
    const paymentsQuery = payment_1.paymentSchema
        .find({}, "id from price category description")
        .where("registerId")
        .equals(registerId)
        .exec((err, payments) => {
        if (err) {
            res.status(404).end();
            return;
        }
        res.json(payments).end();
    });
});
router.post("/payments", (req, res) => {
    const registerId = req.query.payback;
    if (!registerId) {
        res.status(404).end();
        return;
    }
    let payment = req.body;
    const paymentLine = new payment_1.paymentSchema({
        id: payment.id,
        from: payment.from,
        price: payment.price,
        category: payment.category,
        description: payment.description,
        registerId: registerId,
    });
    paymentLine.save().then(() => {
        payment.id = paymentLine._id;
        res.json(payment).end();
    });
});
router.delete("/payments/:id", (req, res) => {
    console.log("payment verwijderen");
    const registerId = req.query.payback;
    if (!registerId) {
        res.status(404).end();
        return;
    }
    const paymentsQuery = payment_1.paymentSchema
        .findByIdAndRemove({ _id: req.params.id })
        .exec((err, result) => {
        console.log(result);
        res.status(200).end();
    });
});
