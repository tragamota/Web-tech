"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PeopleRoutes = void 0;
const express_1 = require("express");
const Person_1 = require("../models/Person");
const router = express_1.Router();
exports.PeopleRoutes = router;
router.get("/pool", (req, res) => {
    const registerId = req.query.payback;
    if (!registerId) {
        res.status(404).end();
        return;
    }
    const personQuery = Person_1.personSchema
        .find({}, "id from price category description")
        .where("registerId")
        .equals(registerId)
        .exec((err, payments) => {
        if (err) {
            res.status(404).end();
            return;
        }
        res.json(payments).end();
    });
});
router.post("/pool", (req, res) => {
    const registerId = req.query.payback;
    console.log("persoon toevoegen");
    console.log(registerId);
    if (!registerId) {
        res.status(404).end();
        return;
    }
    let person = req.body;
    const personLine = new Person_1.personSchema({
        name: person.name,
        registerId: registerId
    });
    personLine.save().then(() => {
        person.id = personLine._id;
        res.json(person).end();
    });
});
router.delete("/pool/:id", (req, res) => {
    console.log("payment verwijderen");
    const registerId = req.query.payback;
    if (!registerId) {
        res.status(404).end();
        return;
    }
    const personQuery = Person_1.personSchema
        .findByIdAndRemove({ _id: req.params.id })
        .exec((err, result) => {
        console.log(result);
        res.status(200).end();
    });
});
