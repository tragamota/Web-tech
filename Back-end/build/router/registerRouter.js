"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RegisterRoutes = void 0;
const express_1 = require("express");
const Person_1 = require("../models/Person");
const register_1 = require("../models/register");
const router = express_1.Router();
exports.RegisterRoutes = router;
router.get("/register", (req, res) => {
    const registerId = req.query.payback;
    if (!registerId) {
        res.status(404).end();
        return;
    }
    const personQuery = Person_1.personSchema
        .find({})
        .where("_id")
        .equals(registerId)
        .exec((err, payments) => {
        if (err) {
            res.status(404).end();
            return;
        }
        res.json(payments).end();
    });
});
router.post("/register", (req, res) => {
    console.log("persoon toevoegen");
    const personLine = new register_1.RegisterSchema({});
    personLine.save().then(() => {
        res.send(personLine._id).end();
    });
});
