import express from "express";
import compression from "compression";
import Cors from "cors";
import Parser from "body-parser";

import mongoose from "mongoose";

import { NextFunction, Request, Response } from "express";

import { PaymentRoutes } from "./router/paymentRouter";
import { PeopleRoutes } from "./router/peopleRouter";
import { RegisterRoutes } from "./router/registerRouter";

// import { CashRegisterDatabase } from "./database/connector"
import { Error } from "./error";
import { TransactionRoutes } from "./router/TransactionRouter";

mongoose.connect('mongodb://localhost:27017/payback', {useNewUrlParser: true, useUnifiedTopology: true});

const App = express();

App.use(compression());
App.use(Cors());
App.use(Parser.json());
App.use(Parser.urlencoded({extended: true,}));

App.use("/", express.static("./public"));
App.use("/api", PaymentRoutes);
App.use("/api", PeopleRoutes);
App.use("/api", RegisterRoutes);
App.use("/api", TransactionRoutes)

App.use("*", (req: Request, res: Response, next: NextFunction) => {
  const error: Error = {
    message: "Endpoint does not exist",
    code: 404,
    Datetime: new Date().toISOString(),
  };
  next(error);
});

App.use((error: Error, req: Request, res: Response, next: NextFunction) => {
  res
    .status(error.code)
    .json({ errorMessage: error.message, errorTime: error.Datetime })
    .end();
});

// console.log(`Database connected?: ${ CashRegisterDatabase.getInstance().createConnections() }`)

var WebServer = App.listen(3000, "0.0.0.0", () => {
  console.log(`Server started on ${ 3000 } `);
});
