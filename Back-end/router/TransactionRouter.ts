import { Router } from "express";
import { Payment, paymentSchema } from "../models/payment";
import Person, { personSchema } from "../models/Person";
import register, { RegisterSchema } from "../models/register";
import Transaction from "../models/Transactions";

const router = Router();

router.get("/transactions", async (req, res) => {
  const registerId = req.query.payback;

  if (!registerId) {
    res.status(404).end();
    return;
  }

  let payments: Array<Payment> = [];
  let pool: Array<Person> = [];
  let transactions: Array<Transaction> = []

  await paymentSchema
    .find({}, "id from price category description")
    .where("registerId")
    .equals(registerId)
    .exec((err, result) => {
      if (err) {
        console.log(err);
      }
      let paymentsLocal = result;

      personSchema
        .find({}, "name")
        .where("registerId")
        .equals(registerId)
        .exec((err, result) => {
          if (err) {
            console.log(err);
          }
          let people = result;

          // algorithm

          people.forEach((element) => {
            pool.push(element.toObject() as Person);
          });

          paymentsLocal.forEach((element) => {
            payments.push(element.toObject() as Payment);
          });

          console.log(pool)
          console.log(payments)

          payments.forEach(element => {
            const transactionPool = pool.filter((x) => x.name !== element.from);

            transactionPool.forEach(element2 => {
                transactions.push({from: element2.name, to: element.from, price: element.price/transactionPool.length})
            });
          });
         
          res.json(transactions).end();
        });
    });
});

export { router as TransactionRoutes };
