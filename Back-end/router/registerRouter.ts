import { Router } from "express";
import Person, { personSchema } from "../models/Person";
import { RegisterSchema } from "../models/register";

const router = Router();

router.get("/register", (req, res) => {
  const registerId = req.query.payback;

  if (!registerId) {
    res.status(404).end();
    return;
  }

  const personQuery = personSchema
    .find({})
    .where("_id")
    .equals(registerId)
    .exec((err, payments) => {
      if (err) {
        res.status(404).end();
        return;
      }

      res.json(payments).end();
    });
});

router.post("/register", (req, res) => {
  console.log("persoon toevoegen");

  const personLine = new RegisterSchema({});

  personLine.save().then(() => {
    res.send(personLine._id).end();
  });
});

export { router as RegisterRoutes };
