import { Router } from "express";
import { Payment, paymentSchema } from "../models/payment";

const router = Router();

router.get("/payments", (req, res) => {
  const registerId = req.query.payback;

  if (!registerId) {
    res.status(404).end();
    return;
  }

  const paymentsQuery = paymentSchema
    .find({}, "id from price category description")
    .where("registerId")
    .equals(registerId)
    .exec((err, payments) => {
      if (err) {
        res.status(404).end();
        return;
      }

      res.json(payments).end();
    });
});

router.post("/payments", (req, res) => {
  const registerId = req.query.payback;

  if (!registerId) {
    res.status(404).end();
    return;
  }

  let payment = req.body as Payment;

  const paymentLine = new paymentSchema({
    id: payment.id,
    from: payment.from,
    price: payment.price,
    category: payment.category,
    description: payment.description,
    registerId: registerId,
  });

  paymentLine.save().then(() => {
    payment.id = paymentLine._id;
    res.json(payment).end();
  });
});

router.delete("/payments/:id", (req, res) => {
  console.log("payment verwijderen");
  const registerId = req.query.payback;

  if (!registerId) {
    res.status(404).end();
    return;
  }

  const paymentsQuery = paymentSchema
    .findByIdAndRemove({ _id: req.params.id })
    .exec((err, result) => {
      console.log(result);
      res.status(200).end();
    });
});

export { router as PaymentRoutes };
