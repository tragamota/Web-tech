import { Component, OnInit } from '@angular/core';
import { PaymentService } from 'src/app/services/paymentService';

import { Payment } from '../../entities/payment';

@Component({
  selector: 'app-payment-overview',
  templateUrl: './payment-overview.component.html',
  styleUrls: ['./payment-overview.component.scss'],
})
export class PaymentOverviewComponent implements OnInit {
  payments: Array<Payment> = [];

  constructor(private paymentService: PaymentService) {}

  ngOnInit(): void {
    this.paymentService.getPayments().subscribe((x) => {
      this.payments = x;
    });
  }
}
