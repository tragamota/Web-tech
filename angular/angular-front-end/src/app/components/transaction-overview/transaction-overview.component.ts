import { Component, OnInit } from '@angular/core';
import Transaction from 'src/app/entities/Transaction';

import { TransactionService } from '../../services/TransactionService';

@Component({
  selector: 'app-transaction-overview',
  templateUrl: './transaction-overview.component.html',
  styleUrls: ['./transaction-overview.component.scss'],
})
export class TransactionOverviewComponent implements OnInit {
  transactions: Array<Transaction> = [];

  constructor(private transactionService: TransactionService) {}

  ngOnInit(): void {
    this.transactionService.getTransactions().subscribe((x) => {
      this.transactions = x;
    });
  }

  getTransactions() {
    this.transactionService.calculateTransaction().subscribe((x) => {
      this.transactions = x;
    })
  }
}
