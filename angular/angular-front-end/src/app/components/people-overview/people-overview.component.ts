import { Component, OnInit } from '@angular/core';
import { PeopleService } from 'src/app/services/peopleService';

import Person from '../../entities/person'

@Component({
  selector: 'app-people-overview',
  templateUrl: './people-overview.component.html',
  styleUrls: ['./people-overview.component.scss']
})
export class PeopleOverviewComponent implements OnInit {
  people: Array<Person> = []

  constructor(private peopleService: PeopleService) { }

  ngOnInit(): void {
    this.peopleService.getPeople().subscribe(x => {
      this.people = x;
    })
  }

}
