import { Component, Input, OnInit } from '@angular/core';
import Person from 'src/app/entities/person';
import { PeopleService } from 'src/app/services/peopleService';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {
  @Input() person: Person;

  constructor(private peopleService: PeopleService) { }

  ngOnInit(): void {
  }

  deletePerson() {
    this.peopleService.deletePerson(this.person);
  }
}
