import { Component, OnInit, Input } from '@angular/core';

import { PaymentService } from '../../services/paymentService';

import { Payment } from '../../entities/payment';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {
  @Input() payment: Payment;

  constructor(private paymentService: PaymentService) {}

  ngOnInit(): void {}

  deletePayment() {
    this.paymentService.deletePayment(this.payment).subscribe(() => {
      console.log("Deleting")
    })
  }
}
