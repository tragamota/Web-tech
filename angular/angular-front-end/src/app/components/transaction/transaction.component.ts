import { Component, Input, OnInit } from '@angular/core';
import Transaction from 'src/app/entities/Transaction';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {
  @Input() transaction: Transaction;

  constructor() { }

  ngOnInit(): void {
  }

}
