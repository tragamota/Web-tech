import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PaymentService } from './services/paymentService';
import { PeopleService } from './services/peopleService';
import { RegisterService } from './services/registerService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'angular-front-end';

  constructor(
    private registerService: RegisterService,
    private route: ActivatedRoute,
    private paymentService: PaymentService,
    private peopleService: PeopleService
  ) {
    registerService.createRegister().subscribe((result) => {
      console.log(result);
      registerService.registerId = result;
      console.log(registerService.registerId);

      this.paymentService.fetchPayments();
      this.peopleService.fetchPool();
    });
  }
}
