import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PaymentOverviewComponent } from './components/payment-overview/payment-overview.component';
import { PaymentComponent } from './components/payment/payment.component';
import { PeopleOverviewComponent } from './components/people-overview/people-overview.component';
import { PersonComponent } from './components/person/person.component';

import { TransactionOverviewComponent } from './components/transaction-overview/transaction-overview.component';
import { TransactionComponent } from './components/transaction/transaction.component';

import { HomeViewComponent } from './views/home-view/home-view.component';
import { PersonCreationComponent } from './views/person-creation/person-creation.component';
import { PaymentCreationComponent } from './views/payment-creation/payment-creation.component';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeViewComponent,
    PaymentOverviewComponent,
    PaymentComponent,
    PeopleOverviewComponent,
    PersonComponent,
    PersonCreationComponent,
    PaymentCreationComponent,
    TransactionOverviewComponent,
    TransactionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
