import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import Person from '../entities/person';
import { RegisterService } from './registerService';

@Injectable({
  providedIn: 'root',
})
export class PeopleService {
  constructor(
    private http: HttpClient,
    private registerService: RegisterService
  ) {}

  private poolUrl = '/api/pool';

  people: Array<Person> = [];

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  fetchPool(): void {
    const url = `${this.poolUrl}/?payback=${this.registerService.registerId}`;
    this.http.get<Array<Person>>(url).subscribe((x) => {
      this.people = x;
    });
  }

  getPeople(): Observable<Array<Person>> {
    return of(this.people);
  }

  getPerson(id: string): Observable<Person> {
    const heroIndex = this.people.findIndex((person) => {
      return person.id === id;
    });

    return of(this.people[heroIndex]);
  }

  addPerson(person: Person): Observable<Person> {
    const url = `${this.poolUrl}?payback=${this.registerService.registerId}`;

    return this.http
      .post<Person>(url, person, this.httpOptions)
      .pipe(tap((x) => this.people.push(x)));
  }

  deletePerson(person: Person): void {
    const url = `${this.poolUrl}/${person.id}?payback=${this.registerService.registerId}`;

    this.http.delete(url, this.httpOptions).subscribe(
      (res) => {
        this.people.splice(
          this.people.findIndex((x) => x.id === person.id),
          1
        );
      },
      (err) => {
        this.people.splice(
          this.people.findIndex((x) => x.id === person.id),
          1
        );
      }
    );
  }
}
