import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root',
})
export class RegisterService {
  private registerUrl = '/api/register';

  registerId: string;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(private http: HttpClient) {}

  doesRegisterExist(registerkey: string): Observable<void> {
    const url = `${this.registerUrl}?payback=${registerkey}`;

    console.log("checking register key")

    return this.http.get<void>(url).pipe(
      tap((x) => {
        console.log(x)
        console.log("register does exist")
        this.registerId = registerkey;
      })
    );
  }

  createRegister(): Observable<string> {
    const url = `${this.registerUrl}`;

    console.log("checking register key");
    return this.http.post<string>(url, '');
  }
}
