import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { Payment, PaymentCategory } from '../entities/payment';
import { RegisterService } from './registerService';

@Injectable({ providedIn: 'root' })
export class PaymentService {
  private paymentsUrl = '/api/payments';

  payments: Array<Payment> = [];

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(private http: HttpClient, private registerService: RegisterService) {

  }

  fetchPayments(): void {
    const url = `${this.paymentsUrl}/?payback=${this.registerService.registerId}`;
    this.http.get<Array<Payment>>(url).subscribe((x) => {
      this.payments = x;
    });
  }

  getPayments(): Observable<Array<Payment>> {
    return of(this.payments);
  }

  getPayment(id: string): Observable<Payment> {
    const heroIndex = this.payments.findIndex((payment) => {
      return payment.id === id;
    });

    return of(this.payments[heroIndex]);
  }

  addPayment(payment: Payment): Observable<Payment> {
    const url = `${this.paymentsUrl}/?payback=${this.registerService.registerId}`;

    return this.http
      .post<Payment>(url, payment, this.httpOptions)
      .pipe(tap((x) => this.payments.push(x)));
  }

  deletePayment(payment: Payment): Observable<Payment> {
    const url = `${this.paymentsUrl}/${payment.id}?payback=${this.registerService.registerId}`;

    console.log(url);

    return this.http.delete<Payment>(url, this.httpOptions).pipe(
      tap(
        (x) =>
          (this.payments.splice(
            this.payments.findIndex((x) => x.id === payment.id),
            1
          ))
      ),
      catchError(
        (x) =>
          (this.payments.splice(
            this.payments.findIndex((x) => x.id === payment.id),
            1
          ))
      )
    );
  }
}
