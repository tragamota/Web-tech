import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import Transaction from '../entities/Transaction';
import { RegisterService } from './registerService';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class TransactionService {
  private transactionUrl = '/api/transactions';

  transaction: Array<Transaction> = [];

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(
    private http: HttpClient,
    private registerService: RegisterService
  ) {}

  getTransactions(): Observable<Array<Transaction>> {
    return of(this.transaction);
  }

  calculateTransaction(): Observable<Array<Transaction>> {
    const url = `${this.transactionUrl}?payback=${this.registerService.registerId}`;

    return this.http.get<Array<Transaction>>(url).pipe(
      tap((x) => {
        this.transaction = x;
      })
    );
  }
}
