import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeViewComponent } from "./views/home-view/home-view.component"
import { PersonCreationComponent } from "./views/person-creation/person-creation.component"
import { PaymentCreationComponent } from "./views/payment-creation/payment-creation.component"
import { RegisterService } from './services/registerService';

const routes: Routes = [
  { path: "", component: HomeViewComponent},
  { path: "people", component: PersonCreationComponent },
  { path: "payment", component: PaymentCreationComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
}
