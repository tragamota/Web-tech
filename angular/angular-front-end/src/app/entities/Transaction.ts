export default interface Transaction {
  from: string;
  to: string;
  price: Number;
}
