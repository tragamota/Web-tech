export interface Payment {
  id?: string;
  from: string;
  price: number;
  category: PaymentCategory;
  description?: string;
}

export enum PaymentCategory {
  Bioscoop = 'Bioscoop',
  Restaurant = 'Restaurant',
  Karting = 'Karten',
  Museum = 'Museum',
  AdvancedPayment = 'Voorgeschoten',
  Other = 'Overig',
}
