import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Payment, PaymentCategory } from 'src/app/entities/payment';
import { Router } from '@angular/router';
import { PaymentService } from 'src/app/services/paymentService';

@Component({
  selector: 'app-payment-creation',
  templateUrl: './payment-creation.component.html',
  styleUrls: ['./payment-creation.component.scss'],
})
export class PaymentCreationComponent implements OnInit {
  paymentForm: FormGroup;

  payment: Payment = {
    from: '',
    category: PaymentCategory.Other,
    price: 0.01,
  };

  constructor(
    private router: Router,
    private paymentService: PaymentService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.paymentForm = new FormGroup({
      from: new FormControl(this.payment.from, [
        Validators.required,
        Validators.minLength(2),
      ]),
      price: new FormControl(this.payment.price, [
        Validators.min(0.01)
      ]),
    })
  }

  onSubmit(): void {
    this.paymentForm.markAllAsTouched();
    console.log(this.paymentForm.valid)
    if(this.paymentForm.invalid) {

      console.log("Invalid form data")
      return;
    }

    this.payment.from = this.from.value;
    this.payment.price = this.price.value;
    console.log(this.payment);

    this.paymentService.addPayment(this.payment).subscribe(
      (result) => {
        console.log('Added: ');
        console.log(result);
        this.router.navigateByUrl('/');
      },
      (error) => {
        console.log(error);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }

  get from() { return this.paymentForm.get('from'); }

  get price() { return this.paymentForm.get('price'); }
}
