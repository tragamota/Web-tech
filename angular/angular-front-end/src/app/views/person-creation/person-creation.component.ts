import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { PeopleService } from 'src/app/services/peopleService';

import Person from 'src/app/entities/person';

@Component({
  selector: 'app-person-creation',
  templateUrl: './person-creation.component.html',
  styleUrls: ['./person-creation.component.scss'],
})
export class PersonCreationComponent implements OnInit {
  person: Person = {
    name: '',
  };

  constructor(
    private router: Router,
    private personService: PeopleService,
    private location: Location
  ) {}

  ngOnInit(): void {}

  onSubmit(): void {
    this.personService.addPerson(this.person).subscribe(
      (result) => {
        console.log('Added: ');
        console.log(result);
        this.router.navigateByUrl('/');
      },
      (error) => {
        console.log(error);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }
}
