import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

import Index from "../views/Index.vue";
import PaymentEntry from "../views/PaymentEntry.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Index",
    component: Index,
  },
  {
    path: "/payment",
    name: "Payment",
    component: PaymentEntry
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
