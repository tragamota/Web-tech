import { Payment, PaymentCategory } from "../../entities/payment";

export type UtilitiesState = {
  paybackRegisterKey: string;
};

export const utilitiesState: UtilitiesState = {
  paybackRegisterKey: "",
};
