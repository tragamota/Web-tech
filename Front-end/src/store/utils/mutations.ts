import { MutationTree } from "vuex";

import { UtilitiesState } from "./state";
import { UtilitiesMutationTypes } from "./mutation-types";

export type Mutations<S = UtilitiesState> = {
  [UtilitiesMutationTypes.CHANGE_REGISTER_KEY](
    state: S,
    paybackRegisterKey: string
  ): void;
};

export const mutations: MutationTree<UtilitiesState> & Mutations = {
  [UtilitiesMutationTypes.CHANGE_REGISTER_KEY] (state: UtilitiesState, paybackRegisterKey: string)
  {
    state.paybackRegisterKey = paybackRegisterKey;
  },
};
