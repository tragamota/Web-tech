import { GetterTree } from "vuex";

import { RootState } from "@/store";
import { UtilitiesState } from "./state";

export type Getters = {
  registerKey(state: UtilitiesState): string;
};

export const getters: GetterTree<UtilitiesState, RootState> & Getters = {
  registerKey: (state) => state.paybackRegisterKey,
};
