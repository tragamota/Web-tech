import axios from "axios";

import { ActionTree, ActionContext } from "vuex";

import { Payment } from "@/entities/payment";

import { RootState } from "@/store";
import { UtilitiesState } from "./state";

import { Mutations } from "./mutations";
import { UtilitiesMutationTypes } from "./mutation-types";
import { UtilitiesActionTypes } from "./action-types";

type AugmentedActionContext = {
  commit<K extends keyof Mutations>(
    key: K,
    payload: Parameters<Mutations[K]>[1]
  ): ReturnType<Mutations[K]>;
} & Omit<ActionContext<UtilitiesState, RootState>, "commit">;

export interface Actions {
  [UtilitiesActionTypes.CHECK_REGISTER_KEY](
    { commit }: AugmentedActionContext,
    registerKey: string
  ): Promise<boolean>;
  [UtilitiesActionTypes.CREATE_NEW_REGISTER](
    { commit }: AugmentedActionContext,
    registerKey: string
  ): Promise<boolean>;
}

export const actions: ActionTree<UtilitiesState, RootState> & Actions = {
  async [UtilitiesActionTypes.CHECK_REGISTER_KEY](
    { commit },
    registerKey: string
  ) 
  {
    return new Promise(() => {
      setTimeout(async () => {
        const data = (await axios
          .post("/api/register", registerKey)
          .then((ans) => {
            return JSON.parse(ans.data);
          })
          .catch((err) => {
            return "";
          })) as any;

        if (data.registerKey) {
          commit(UtilitiesMutationTypes.CHANGE_REGISTER_KEY, data);
        } else {
          // this.CREATE_NEW_REGISTER()
        }

        return true;
      }, 5000);
    });
  },
  async [UtilitiesActionTypes.CREATE_NEW_REGISTER](
    { commit },
    registerKey: string
  ) 
  {
    return new Promise(() => {
      setTimeout(async () => {
        const data = (await axios
          .post("/api/register", registerKey)
          .then((ans) => {
            return JSON.parse(ans.data);
          })
          .catch((err) => {
            return "";
          })) as any;

        if (data.registerKey) {
          commit(UtilitiesMutationTypes.CHANGE_REGISTER_KEY, data);
        } else {
          // this.CREATE_NEW_REGISTER();
        }

        return true;
      }, 5000);
    });
  },
};
