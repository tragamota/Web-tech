import { Payment, PaymentCategory } from "../../entities/payment";

export type PaymentState = {
  payments: Array<Payment>;
};

export const paymentState: PaymentState = {
  payments: [
    { id: "1", from: "Ian", price: 50.50, category: PaymentCategory.Other },
    { id: "2", from: "Ian", price: 50.50, category: PaymentCategory.Karting },
    {
      id: "3",
      from: "Ian",
      price: 50.5,
      category: PaymentCategory.Other,
      description: "aaaaaaa",
    },
    { id: "4", from: "Ian", price: 50.50, category: PaymentCategory.Other },
  ],
};
