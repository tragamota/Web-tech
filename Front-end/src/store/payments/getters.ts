import { GetterTree } from "vuex";

import { Payment } from "@/entities/payment";

import { RootState } from "@/store";
import { PaymentState } from "./state";

export type Getters = {
  payments(state: PaymentState): Array<Payment>;
};

export const getters: GetterTree<PaymentState, RootState> & Getters = {
  payments: (state) => state.payments,
};
