import { MutationTree } from "vuex";

import { Payment } from "@/entities/payment";

import { PaymentState } from "./state";
import { PaymentMutationTypes } from "./mutation-types";

export type Mutations<S = PaymentState> = {
  [PaymentMutationTypes.ADD_PAYMENT](state: S, payment: Payment): void;
  [PaymentMutationTypes.REMOVE_PAYMENT](state: S, payment: Payment): void;
  [PaymentMutationTypes.UPDATE_PAYMENT](state: S, payment: Payment): void;
};

export const mutations: MutationTree<PaymentState> & Mutations = {
  [PaymentMutationTypes.ADD_PAYMENT](state: PaymentState, payment: Payment) {
    state.payments.push(payment);
  },
  [PaymentMutationTypes.REMOVE_PAYMENT](state: PaymentState, payment: Payment) {
    const paymentIndex = state.payments.findIndex(
      (element) => element.id === payment.id
    );
    console.log(payment.id);
    console.log(paymentIndex);

    if (paymentIndex > -1)
      state.payments.splice(paymentIndex, 1);
  },
  [PaymentMutationTypes.UPDATE_PAYMENT](state: PaymentState, payment: Payment) {
    const paymentIndex = state.payments.findIndex(
      (element) => element.id === payment.id
    );

    state.payments[paymentIndex] = payment;
  },
};
