import axios from "axios";

import { ActionTree, ActionContext } from "vuex";

import { Payment } from "@/entities/payment";

import { RootState } from "@/store";
import { PaymentState } from "./state";

import { Mutations } from "./mutations";
import { PaymentMutationTypes } from "./mutation-types";
import { PaymentActionTypes } from "./action-types";
import { resolveComponent } from "@vue/runtime-core";

type AugmentedActionContext = {
  commit<K extends keyof Mutations>(
    key: K,
    payload: Parameters<Mutations[K]>[1]
  ): ReturnType<Mutations[K]>;
} & Omit<ActionContext<PaymentState, RootState>, "commit">;

export interface Actions {
  [PaymentActionTypes.FETCH_PAYMENTS](
    { commit }: AugmentedActionContext,
    someId: string
  ): Promise<boolean>;
  [PaymentActionTypes.CREATE_PAYMENT](
    { commit }: AugmentedActionContext,
    paymentToAdd: Payment
  ): Promise<void>;
  [PaymentActionTypes.DELETE_PAYMENT](
    { commit }: AugmentedActionContext,
    paymentToRemove: Payment
  ): Promise<void>;
}

export const actions: ActionTree<PaymentState, RootState> & Actions = {
  async [PaymentActionTypes.FETCH_PAYMENTS]({ commit }, someId: string) {
    return new Promise(() => {
      setTimeout(async () => {
        const data = (await axios.get("/api/payments").catch((err) => {
          return [];
        })) as Array<Payment>;

        data.forEach((element) => {
          commit(PaymentMutationTypes.ADD_PAYMENT, element);
        });

        return true;
      }, 5000);
    });
  },
  async [PaymentActionTypes.CREATE_PAYMENT]({ commit }, paymentToAdd: Payment) {
    return new Promise((resolve, reject) => {
      return axios
        .post("api/payments", paymentToAdd)
        .then((response) => {
          return response.data as Payment;
        })
        .then((payment) => {
          commit(PaymentMutationTypes.ADD_PAYMENT, payment);
          resolve();
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  async [PaymentActionTypes.DELETE_PAYMENT](
    { commit },
    paymentToDelete: Payment
  ) {
    return new Promise((resolve, reject) => {
      return axios
        .delete(`api/payments/${paymentToDelete.id}`)
        .then((response) => {
          return response.status === 200;
        })
        .catch((err) => {
          reject("Payment is not deleted");
        })
        .then((isDeleted) => {
          if (isDeleted) {
            commit(PaymentMutationTypes.REMOVE_PAYMENT, paymentToDelete);
            resolve();
          }

          throw new Error("Payment is not deleted");
        })
        .catch((err) => {
          commit(PaymentMutationTypes.REMOVE_PAYMENT, paymentToDelete);
          reject(err);
        });
    });
  },
};
