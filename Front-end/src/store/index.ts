import { createStore } from "vuex";

import {
  store as payments,
  PaymentStore,
  PaymentState,
} from "@/store/payments";

export type RootState = {
  payments: PaymentState;
};

export type Store = PaymentStore<Pick<RootState, "payments">>;

export default createStore({
  modules: {
    payments,
  },
});
